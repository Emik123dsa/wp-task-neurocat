# Neurocat Task

This assignment is currently powered by Wordpress REST API services as Backend and React with SSR for front-end part.

## Installation process

Don't forget to install before:

- [make]: Make GNU
- [docker]: Docker && Docker-Compose

This command will be outputed Back-End and Front-End services with docker-compose helpers:

```sh
make all
```

If you just want to run Front-End Service:

```sh
make neurocat-front-end
```

If you want to run only Back-End service:

```sh
make neurocat-back-end
```

After go to the browser:

- [back-end]
- [front-end]

## Wordpress authentication

Wordpress default admin user:

- Login: example@example.com
- Password: iUa2XAAV3f6Mz/qD

## Run front-end

You can also run front-end separately, but it won't work without enabled Back-End:

```sh
yarn run build-frontend && yarn run start-frontend
```

## Development

```sh
make neurocat-back-end
```

```sh
yarn install && yarn run dev-frontend
```

## Testing

This command will testing spec components via Jest:

```sh
yarn install && yarn run tests
```

[make]: https://www.gnu.org/software/make/
[docker]: https://www.docker.com/
[front-end]: http://localhost:3000/
[back-end]: http://localhost/wp-admin/
