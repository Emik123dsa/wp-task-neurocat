NEUROCAT_FRONTEND 	:= neurocat-front-end
NEUROCAT_BACKEND 	:= neurocat-back-end

ALL 	:= all

default: $(ALL)

$(ALL): $(NEUROCAT_BACKEND) $(NEUROCAT_FRONTEND)

$(NEUROCAT_BACKEND): 
	cd ./packages/app-backend/ && make backend

$(NEUROCAT_FRONTEND): 
	cd ./packages/app-frontend/ && make frontend