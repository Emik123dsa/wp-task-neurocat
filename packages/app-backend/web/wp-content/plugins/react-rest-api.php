<?php
/*
    Plugin Name: React Rest Api Services
    Plugin URI: -
    Description: Intsance plugin for fetching dynamic information 
    Author: Em1k
    Version: 0.0.1
    Author URI: Em1k
*/

class React_REST_Controller
{
    private string $namespace;
    private string $version;

    const POST_STATUSES = ["publish", "draft"];
    /*
     * Define rest controller variables,
     * we actually can alter required
     * details in the furthed development
     *
     * @param string $namespace
     * @param string $version
     */
    public function __construct(string $namespace, string $version)
    {
        $this->namespace = $namespace;
        $this->version = $version;
    }
    /**
     * Register and define root schematics
     * That's required to make only 4 routes
     * to fetch all of the content to the
     * application
     *
     * @return void
     */
    public function register_routes()
    {
        $prefix = sprintf("%s/v%s", $this->namespace, $this->version);

        register_rest_route($prefix, "/getHeaderContent", [
            "methods" => WP_REST_Server::READABLE,
            "callback" => [$this, "retrieve_header_items"],
            "update_callback" => null,
            "schema" => null,
            "args" => [],
        ]);

        register_rest_route($prefix, "/getAppContent", [
            "methods" => WP_REST_Server::READABLE,
            "callback" => [$this, "retrieve_category_content"],
            "update_callback" => null,
            "schema" => null,
            "args" => [],
        ]);
    }
    /**
     * Retrieve categories content
     *
     * @param WP_REST_Request $request
     * @return WP_REST_Response
     */
    public function retrieve_category_content(
        WP_REST_Request $request
    ): ?WP_REST_Response {
        $categories = $this->prepare_get_categories_location();

        if (!(is_array($categories) && count($categories) > 0)) {
            return new WP_Error(400, "Bad Request");
        }

        $callback = function (&$category) use ($categories) {
            $categoryMutated = [];
            $args = wp_parse_args(["cat" => $category->cat_ID]);
            $query = new WP_Query($args);
            $categoryMutated["categoryId"] = $category->cat_ID;
            $categoryMutated["categoryName"] = $category->name;
            if (
                property_exists($category, "parent") &&
                $category->parent === 0
            ) {
                /**
                 * We can create categoryDetails schema
                 * to be entirely sure that there's existing
                 * some children categories
                 *
                 * @array categoryDetails
                 */
                $categoryMutated["categoryChildren"] = array_map(function (
                    $item
                ) use ($category) {
                    if (
                        !strcmp($item->parent, $category->cat_ID) &&
                        strcmp($item->slug, $category->slug)
                    ) {
                        $categoryChildren = [];
                        $categoryChildren["categoryId"] = $item->cat_ID;

                        $categoryChildren["categoryParent"] = $item->parent;
                        $categoryChildren["categoryName"] = $item->name;
                        $categoryChildren["categoryDescription"] =
                            $item->description;

                        $childrenQuery = new WP_Query(
                            wp_parse_args([
                                "cat" => $item->cat_ID,
                            ])
                        );

                        $childrenPosts = new \ArrayIterator(
                            $childrenQuery->posts
                        );

                        if ($childrenQuery->have_posts()) {
                            iterator_apply(
                                $childrenPosts,
                                function ($childrenPost, &$categoryChildren) {
                                    $currentChildrenPost = $childrenPost->current();
                                    if (
                                        strcmp(
                                            $categoryChildren->post_status,
                                            self::POST_STATUSES[0]
                                        )
                                    ) {
                                        $categoryChildrenID =
                                            $currentChildrenPost->ID;
                                        $categoryChildren["categoryDetails"][
                                            $categoryChildrenID
                                        ]["postTitle"] =
                                            $currentChildrenPost->post_title;
                                        $categoryChildren["categoryDetails"][
                                            $categoryChildrenID
                                        ]["postStatus"] =
                                            $currentChildrenPost->post_status;
                                        $categoryChildren["categoryDetails"][
                                            $categoryChildrenID
                                        ][
                                            "postContent"
                                        ] = $this->apply_content_cut_filter(
                                            $currentChildrenPost->post_content
                                        );
                                        $categoryChildren["categoryDetails"][
                                            $categoryChildrenID
                                        ]["postDescription"] =
                                            $currentChildrenPost->post_excerpt;
                                    }
                                    return true;
                                },
                                [$childrenPosts, &$categoryChildren]
                            );
                        }
                        wp_reset_postdata();
                        $categoryChildren["categoryDetails"] = array_filter(
                            array_values($categoryChildren["categoryDetails"])
                        );
                        return $categoryChildren;
                    }
                },
                $categories);
                $categoryMutated["categoryChildren"] = array_values(
                    array_filter($categoryMutated["categoryChildren"])
                );

                if (empty($categoryMutated["categoryChildren"])) {
                    unset($categoryMutated["categoryChildren"]);
                }
                $posts = new \ArrayIterator($query->posts);

                if ($query->have_posts()) {
                    iterator_apply(
                        $posts,
                        function ($post, &$categoryMutated) {
                            $currentPost = $post->current();
                            if (!isset($categoryMutated["categoryChildren"])) {
                                $categoryMutated["categoryDetails"][
                                    $currentPost->ID
                                ]["postTitle"] = $currentPost->post_title;
                                $categoryMutated["categoryDetails"][
                                    $currentPost->ID
                                ][
                                    "postContent"
                                ] = $this->apply_content_cut_filter($currentPost->post_content);
                                $categoryMutated["categoryDetails"][
                                    $currentPost->ID
                                ][
                                    "postStatus"
                                ] = $this->apply_content_cut_filter($currentPost->post_status);
                                $categoryMutated["categoryDetails"][
                                    $currentPost->ID
                                ][
                                    "postDescription"
                                ] = $currentPost->post_excerpt;
                            }
                            return true;
                        },
                        [$posts, &$categoryMutated]
                    );
                }

                if (isset($categoryMutated["categoryDetails"])) {
                     $categoryMutated["categoryDetails"] = array_values($categoryMutated["categoryDetails"]);
                }

                wp_reset_postdata();

                return $categoryMutated;
            }
        };

        return new WP_REST_Response([
            "status" => 200,
            "message" => null,
            "data" => (array) array_values(
                array_filter(array_map($callback, $categories))
            ),
            "get" => $this->get_request_credentials($request),
        ]);
    }
    /**
     * Apply content cut filter
     *
     * @param string $payload
     * @return string
     */
    private function apply_content_cut_filter(string $payload): string
    {
        $callback = function () use ($payload) {
            $content = force_balance_tags($payload);
            $content = preg_replace(
                "/[!?<][a-zA-Z][!?>]|[][!?<][?!\!\/][-]{0,2}[a-zA-Z\:\/\\ ]+[-]{0,2}[!?>]/",
                "",
                $content
            );
            $content = preg_replace("/\n/", "", $content);
            $content = preg_replace(
                "/[!?<].*[!?>](.*)[!?<].*[!?>]/",
                "$1",
                $content
            );
            $content = preg_replace("/&amp; /", "", $content);
            return $content;
        };
        add_filter("the_content", $callback, 20, 1);
        return apply_filters("the_content", $payload);
    }
    /**
     * Retrieve header items
     * according to the request
     *
     * @return void
     */
    public function retrieve_header_items(
        WP_REST_Request $request
    ): ?WP_REST_Response {
        $locations = get_pages();

        if (!(is_array($locations) && count($locations) > 0)) {
            return new WP_REST_Response(
                [
                    "status" => 400,
                    "message" => __("Bad Request"),
                    "data" => null,
                ],
                400
            );
        }

        $items = array_map(function ($item) {
            $menu = [];
            if (!strcmp($item->post_status, self::POST_STATUSES[0])) {
                $menu["ID"] = $item->ID;
                $menu["title"] = $item->post_title;
                $menu["name"] = $item->post_name;
                $menu["order"] = $item->menu_order;
            }
            return $menu;
        }, $locations);

        array_multisort(
            array_map(function ($item) {
                return $item["order"];
            }, $items),
            SORT_ASC,
            SORT_NUMERIC,
            $items
        );
        return new WP_REST_Response(
            [
                "status" => 200,
                "message" => null,
                "data" => $items,
                "get" => $this->get_request_credentials($request),
            ],
            200
        );
    }
    /**
     * Get request parameters
     * according to the user request
     *
     * @param [type] $request
     * @return array
     */
    private function get_request_credentials($request): array
    {
        return $request->get_params();
    }
    /**
     * Preparing categories
     *
     * @return array
     */
    private function prepare_get_categories_location(): array
    {
        return get_categories();
    }
    /**
     * Get headers information,
     * so, we can use it dynamically in the
     * application
     *
     * @return array
     */
    private function prepare_get_nav_menu_location(): array
    {
        return get_nav_menu_locations();
    }
}

function prepare_react_custom_routes()
{
    $reactController = new React_REST_Controller("react", "2");
    $reactController->register_routes();
}

add_action("rest_api_init", "prepare_react_custom_routes");

?>
