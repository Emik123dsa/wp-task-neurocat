/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import http from 'http';
import express from 'express';
import * as path from 'path';
import * as webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';

import ɵwebpackConfig from '../webpack.config';
import webpackConfig from '../webpack.config';

const PORT: number = Number(process.env.NODE_PORT) || 4200;
const HOST: string | null = process.env.NODE_HOST || '127.0.0.1';
let webpackDevServer: WebpackDevServer;
let webpackClientPort: number = PORT;
webpackClientPort = ++webpackClientPort;

enum ɵSSR {
  client = 'client',
  server = 'server',
}

type SSR = Record<'server' | 'client', webpack.Configuration>;

const ɵprepareListener: () => Promise<void> = (): Promise<void> =>
  new Promise(
    (
      res: (value: void | PromiseLike<void>) => void,
      rej: (err?: unknown) => void,
    ) => {
      if (!ɵwebpackConfig) {
        return rej(ɵwebpackConfig);
      }

      const ɵwebpackConfigKeys: (string | symbol | number)[] =
        Reflect.ownKeys(ɵwebpackConfig) || null;

      if (
        ɵwebpackConfigKeys.includes(ɵSSR.client) &&
        ɵwebpackConfigKeys.includes(ɵSSR.server)
      ) {
        return res();
      }

      return rej(ɵwebpackConfigKeys);
    },
  );

const serverConfig: webpack.Configuration =
  webpackConfig && (webpackConfig as SSR).server;

const clientConfig: webpack.Configuration =
  webpackConfig && (webpackConfig as SSR).client;

(clientConfig.entry as string[]).unshift(
  `webpack-dev-server/client?http://localhost:${webpackClientPort}`,
  'webpack/hot/only-dev-server',
);

let serverOutput: string | null = null;
const ɵserverListener: () => Promise<void> = (): Promise<void> => {
  try {
    const serverCompiler = webpack.default(serverConfig);
    serverOutput = path.resolve(
      serverCompiler.options.output.path,
      'server.js',
    );
    serverCompiler.watch(
      serverConfig.watchOptions || {
        aggregateTimeout: 200,
      },
      ɵstartSSRServer,
    );

    return Promise.resolve();
  } catch (e) {
    return Promise.reject();
  }
};

let ɵsseConnections: (http.ServerResponse | null)[] = [];

const ɵsseMiddleware = (req: http.ClientRequest, res: http.ServerResponse) => {
  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    Connection: 'keep-alive',
  });
  ɵsseConnections.push(res);
};

const ɵdispatchEvent = (type: string, data?: unknown): void => {
  ɵsseConnections.forEach((conn: http.ServerResponse) => {
    conn.write(`data: ${JSON.stringify({ type, data: data || {} })}\n\n`);
  });
};

const ɵcloseConnection = () => {
  ɵsseConnections.forEach((conn: http.ServerResponse) => {
    conn.end();
  });
  ɵsseConnections = [];
};

let ɵinitialListeners: (http.ServerResponse | null)[] = [];

function initialRequestListener(
  req: http.IncomingMessage,
  res: http.ServerResponse,
): void {
  res.writeHead(200, {
    'Content-Type': 'text/html',
  });
  res.write('init');
  ɵinitialListeners.push(res);
}

const ɵsendToListeners = (message: string | null): void => {
  ɵinitialListeners.forEach(conn => conn.write(message));
};

const ɵcloseListeners = (): void => {
  ɵinitialListeners.forEach(conn => conn.end());
  ɵinitialListeners = [];
};

const httpServer: http.Server = http.createServer(initialRequestListener);
httpServer.listen(PORT);
let isStarted = false;
let expressRef: express.Express = null;

const ɵstartSSRServer = (
  err: webpack.WebpackError,
  stats: webpack.Stats,
): void => {
  let ɵerrorsFactory: Record<string, unknown>[] = null;
  if (err?.stack || err) {
    if (err?.details) {
      ɵerrorsFactory = err?.details;
    }
  } else if (stats.hasErrors()) {
    ɵerrorsFactory = stats?.toJson()?.errors;
  }

  if (ɵerrorsFactory) {
    delete require.cache[require.resolve(serverOutput)];
    return ɵdispatchEvent('error', ɵerrorsFactory);
  }

  if (isStarted) {
    delete require.cache[require.resolve(serverOutput)];
    const { app }: any = require(serverOutput ?? null);
    const {
      default: expressInitNextApp,
      reactServerInit,
    }: Record<'default' | 'reactServerInit', any> = app;
    ɵdispatchEvent('refresh');
    ɵcloseConnection();
    reactServerInit([ɵsseMiddleware]);
    httpServer.removeListener('request', expressRef);
    httpServer.on('request', expressInitNextApp);
    expressRef = expressInitNextApp as express.Express;
    return;
  }

  isStarted = true;
  const { app }: any = require(serverOutput ?? null);
  const {
    default: expressInitApp,
    reactServerInit,
  }: Record<'default' | 'reactServerInit', any> = app;
  expressRef = expressInitApp as express.Express;
  reactServerInit([ɵsseMiddleware]);
  httpServer.removeListener('request', initialRequestListener);
  httpServer.on('request', expressInitApp);
  ɵsendToListeners('done');
  ɵcloseListeners();
  return;
};

const ɵclientListener: () => Promise<void> = (): Promise<void> => {
  try {
    const clientCompiler: webpack.Compiler = webpack.default(clientConfig);
    webpackDevServer = new WebpackDevServer(clientCompiler, {
      disableHostCheck: true,
      clientLogLevel: 'info',
      hot: true,
      public: `localhost:${webpackClientPort}`,
      hotOnly: true,
      noInfo: true,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers':
          'X-Requested-With, content-type, Authorization',
        'Access-Control-Allow-Methods':
          'GET, POST, PUT, DELETE, PATCH, OPTIONS',
      },
    });
    webpackDevServer.listen(webpackClientPort, HOST);

    return Promise.resolve();
  } catch (e) {
    return Promise.reject(e);
  }
};

const ɵstart: () => Promise<void> = async (): Promise<void> => {
  try {
    await Promise.all([
      ɵprepareListener(),
      ɵclientListener(),
      ɵserverListener(),
    ]);
  } catch (e) {
    return Promise.reject();
  }
};

ɵstart().then(
  (): void => {
    ɵsendToListeners('start');
  },
  (): void => {
    ɵsendToListeners('error');
    if (webpackDevServer) {
      webpackDevServer.close();
    }
  },
);
