/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/prefer-regexp-exec */

import path from 'path';
import http from 'http';
import fs from 'fs';

const PORT: number = +process.env.PORT || 3000;
const HOST: string = process.env.HOST || '0.0.0.0';

function getFilesFromPath(path: string, extension: string): string | string[] {
  const files = fs.readdirSync(path);
  return files.filter(file =>
    // eslint-disable-next-line no-useless-escape
    file.match(new RegExp(`.*\.(${extension})`, 'ig')),
  );
}

const serverStatic: string | string[] =
  getFilesFromPath('dist/server', '.main.js') &&
  getFilesFromPath('dist/server', '.main.js')[0];

const { app }: any = require(path.resolve(
  process.cwd(),
  `dist/server/${serverStatic}`,
));

const {
  default: expressInitApp,
  reactServerInit,
}: Record<'default' | 'reactServerInit', any> = app;

reactServerInit();
const server: http.Server = http.createServer(expressInitApp);

server.listen(PORT, HOST, () =>
  console.log(`Server has been started on: ${HOST}:${PORT}`),
);
