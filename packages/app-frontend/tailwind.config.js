module.exports = {
  purge: ['./src/**/*.ts', './src/**/*.tsx', './src/**/*.jsx', './src/**/*.js'],
  darkMode: false,
  theme: {
    extend: {
      padding: {
        width: '9rem',
      },
      boxShadow: {
        width: '0 16px 48px rgb(20 21 22 / 50%)',
      },
    },
    colors: {
      error: '#33aaca',
      white: '#fff',
      gray: 'rgba(69,76,76,0.5)',
      item: 'rgba(255,255,255,0.01)',
      dark: '#768696',
      blue: '#33aaca',
      green: '#4bb8b4',
      background: '#141516',
      purple: {
        50: '#505fff',
        100: '#4353FF',
      },
      overlay: '#1e1f21',
    },
    borderColors: {
      white: '#fff',
      dark: '#768696',
      blue: '#33aaca',
      green: '#4bb8b4',
      background: '#141516',
      purple: {
        50: '#505fff',
        100: '#4353FF',
      },
      overlay: '#1e1f21',
    },
    fontSize: {
      tiny: '12px',
      xs: '14px',
      sm: '18px',
      base: '20px',
      md: '24px',
      lg: '32px',
      xl: '40px',
      '2xl': '48px',
    },
    fontFamily: {
      'heebo-regular': ['Heebo Regular', 'sans-serif'],
      'heebo-medium': ['Heebo Medium', 'sans-serif'],
      'heebo-bold': ['Heebo Bold', 'sans-serif'],
      'titillium-regular': ['TitilliumWeb Regular', 'sans-serif'],
      'titillium-light': ['TitilliumWeb Light', 'sans-serif'],
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
