/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import * as path from 'path';
import * as webpack from 'webpack';
import * as WebpackDevServer from 'webpack-dev-server';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import { WebpackManifestPlugin } from 'webpack-manifest-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import CssMinimizerPlugin from 'css-minimizer-webpack-plugin';
import TerserWebpackPlugin from 'terser-webpack-plugin';

/**
 * Disable ECMA Loading
 * with standard require
 **/
const WebpackBarPlugin = require('webpackbar');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const isDev: boolean = process.env.NODE_ENV === 'development';
const isDevServer: boolean = process.env.NODE_DEV_SERVER === 'true';

export type DevServer = 'devServer';
export type SSR = 'client' | 'server';

export const devServer: Record<DevServer, WebpackDevServer.Configuration> = {
  devServer: {
    contentBase: isDevServer
      ? path.resolve(process.cwd(), 'dist/client')
      : undefined,
    compress: true,
    port: isDevServer ? 4200 : 4201,
    open: isDevServer ? true : false,
    noInfo: false,
    quiet: false,
    hot: true,
    historyApiFallback: true,
  },
};

export const resolve: webpack.ResolveOptions = {
  alias: {
    'react-dom': '@hot-loader/react-dom',
    '@': path.resolve(__dirname, './src/app'),
    '~': path.resolve(__dirname, './src/'),
  },
  extensions: ['.ts', '.tsx', '.js', '.jsx', '.scss', '.css', '.sass'],
};

export const entryClientFiles: string[] = [
  './src/main.tsx',
  './src/assets/styles/_main.scss',
  './src/assets/styles/global/_global.scss',
];

export const entryServerFiles: string[] = [
  './src/polyfills.ts',
  './src/server.tsx',
];

export const cssLoaders: webpack.RuleSetRule[] = [
  {
    loader: 'postcss-loader',
    options: {
      postcssOptions: {
        config: path.resolve(process.cwd(), './postcss.config.js'),
      },
      sourceMap: true,
    },
  },
  {
    loader: 'sass-loader',
    options: {
      sassOptions: {
        outputStyle: 'compressed',
      },
      sourceMap: true,
    },
  },
  {
    loader: 'sass-resources-loader',
    options: {
      resources: [
        'src/assets/styles/_variables.scss',
        'src/assets/styles/_colors.scss',
      ],
    },
  },
];

const plugins: webpack.WebpackPluginInstance[] = [
  new WebpackManifestPlugin(),
  // new ForkTsCheckerWebpackPlugin(),
].concat(!isDev ? [new CssMinimizerPlugin(), new TerserWebpackPlugin()] : []);

export const serverPlugins: webpack.WebpackPluginInstance[] = [
  ...plugins,
  new CleanWebpackPlugin(),
  new WebpackBarPlugin({
    name: process.env.NODE_CUSTOM_SERVER_TITLE || 'Server',
    color: 'red',
  }),
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV),
    },
    __SERVER__: true,
    __CLIENT__: false,
  }),
];

export const clientPlugins: webpack.WebpackPluginInstance[] = [
  ...plugins,
  new CleanWebpackPlugin(),
  new MiniCssExtractPlugin({
    filename: isDev ? '[chunkhash:4].dev.css' : '[contenthash].css',
    chunkFilename: isDev ? '[chunkhash:4].dev.css' : '[contenthash].css',
  }),
  new WebpackBarPlugin({
    name: process.env.NODE_CUSTOM_CLIENT_TITLE || 'Client',
    color: 'blue',
  }),
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV),
    },
    __SERVER__: false,
    __CLIENT__: true,
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(__dirname, 'public/index.html'),
    minify: {
      removeComments: !isDev,
      collapseWhitespace: !isDev,
    },
    hash: true,
    inject: true,
  }),
];

export const moduleLoader: (schema: SSR) => webpack.Configuration = (
  schema: SSR,
) => {
  const rules: webpack.RuleSetRule[] = [];

  rules.push(
    {
      test: /\.(ts|tsx)$/i,
      exclude: /node_modules/,
      use: [
        {
          loader: 'babel-loader',
          options: {
            plugins: isDev ? ['react-hot-loader/babel'] : undefined,
          },
        },
        {
          loader: 'eslint-loader',
        },
      ],
    },
    {
      test: /\.(png|img|eot|otf|ttf|woff|woff2)$/i,
      exclude: /node_modules/,
      use: [{ loader: 'file-loader' }],
    },

    {
      test: /\.svg$/i,
      exclude: /node_modules/,
      use: [
        {
          loader: 'svg-react-loader',
        },
      ],
    },
    {
      test: /\.(mp4|webm|gif)$/i,
      exclude: /node_modules/,
      use: [
        {
          loader: 'url-loader',
          options: {
            limit: 8192,
          },
        },
      ],
    },
    {
      test: /\.html$/i,
      exclude: /node_modules/,
      use: 'html-loader',
    },
  );

  if (schema === 'client') {
    rules.push(
      {
        test: /\.(sa|sc|c)ss$/i,
        sideEffects: true,
        exclude: /node_modules/,
        include: path.resolve(process.cwd(), 'src/assets/styles/global'),
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          'css-loader',
          ...cssLoaders,
        ],
      },
      {
        test: /\.(sa|sc|c)ss$/i,
        exclude: path.resolve(process.cwd(), 'src/assets/styles/global'),
        sideEffects: true,
        use: [
          {
            loader: isDev ? 'style-loader' : MiniCssExtractPlugin.loader,
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              modules: {
                localIdentName: isDev
                  ? '[name]_[local]_[hash:base64:5]'
                  : '[hash:base64:5]',
              },
            },
          },
          ...cssLoaders,
        ],
      },
    );
  }

  return {
    module: {
      rules,
    },
  };
};

export const optimization: webpack.Configuration = {
  optimization: {
    splitChunks: {
      cacheGroups: {
        styles: {
          name: 'styles',
          type: 'css/mini-extract',
          chunks: 'all',
          enforce: true,
        },
      },
    },
  },
};
