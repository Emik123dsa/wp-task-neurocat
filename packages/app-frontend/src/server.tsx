import * as React from 'react';
import express from 'express';
import * as fs from 'fs';
import AppServer from './app/app.server.module';
import { renderToString } from 'react-dom/server';
import { Url } from 'node:url';
import * as parseUrl from 'parseurl';
import { HtmlService } from './app/core/services/html.service';
import { ɵroutes } from '@/app.routing-module';
import { RouteConfig } from 'react-router-config';
import { matchPath } from 'react-router';
import { RequestInitialDataImpl } from './app/core/models/request.initial.data.model';
import { Observable } from 'rxjs/internal/Observable';
import * as path from 'path';
import { serverTemplateOutlet } from '@/shared/helpers/server.template.outlet';
import { ServerStatusCodeImpl } from '@/core/models/server.model';
import { ServerInit } from './app/core/services/server.init.service';
import { ServerInitialDataImpl } from './app/core/models/server.model';
const server: express.Express = express();

const isDevSSR: boolean = process.env.NODE_ENV === 'development';

const STREAM_EVENT_SOURCE: string | null = '/event-stream';

export const reactServerInit: (middlewares: express.Application[]) => void = (
  middlewares: express.Application[],
): void => {
  /**
   * @Enabled events-stream middleware
   * it's allowing us to fetch and resolve
   * files changed and recompiled via webpack
   * @Only For Dev SSR
   */
  if (isDevSSR) {
    if (middlewares) {
      middlewares.forEach((middleware: express.Application) => {
        server.use(STREAM_EVENT_SOURCE, middleware);
      });
    }
  }

  const $rootPath: string | null = path.resolve(process.cwd(), 'dist/server');
  /**
   * @Static server folder
   **/
  server.set('view engine', 'html');
  server.set('views', express.static($rootPath));
  server.get('*.*', express.static($rootPath, {}));
  if (!isDevSSR) {
    const $clientPath: string | null = path.resolve(
      process.cwd(),
      'dist/client',
    );
    server.get('*.*', express.static($clientPath, {}));
  }
  /**
   * @Dispatched React routes via server
   **/
  server.use('*', (req: express.Request, res: express.Response): void => {
    const url: string | null = req.originalUrl || req.url;

    let context: unknown = {};

    const location: Url = parseUrl.original(req);

    const currentRoute: RouteConfig &
      ServerStatusCodeImpl = ɵroutes.find(({ path }: RouteConfig) =>
      matchPath(url, path),
    );

    const serverInit: ServerInit = ServerInit.getInstance();

    serverInit.onServerInit().subscribe(
      (serverData: ServerInitialDataImpl) => {
        context = Object.assign({}, serverData || {});
        if (
          currentRoute &&
          (currentRoute?.component as RequestInitialDataImpl).requestInitialData
        ) {
          const initialRequestData: Observable<unknown> = (currentRoute?.component as RequestInitialDataImpl).requestInitialData();

          initialRequestData.subscribe(
            (data: unknown) => {
              context = Object.assign({}, data || {}, context);
              if (isDevSSR) {
                const htmlService: HtmlService = new HtmlService();
                htmlService.getHTML().subscribe((html: string) => {
                  const appContent: string | null = renderToString(
                    <AppServer location={location} context={context} />,
                  );
                  return res.status(currentRoute.statusCode || 200).end(
                    serverTemplateOutlet({
                      data: html,
                      appContent,
                      context: context || {},
                    }),
                  );
                });
              } else {
                const pathToTemplate: string | null = path.resolve(
                  process.cwd(),
                  'dist/client/index.html',
                );

                fs.readFile(pathToTemplate, 'utf-8', (_error, html) => {
                  if (_error) {
                    if (
                      +(_error as { message: string | null }).message === 404
                    ) {
                      res.status(404).send('Page not found | 404');
                    } else {
                      res.status(500).send('Internal server error | 500');
                    }
                  }

                  const appContent: string | null = renderToString(
                    <AppServer location={location} context={context} />,
                  );

                  return res.status(currentRoute.statusCode || 200).end(
                    serverTemplateOutlet({
                      data: html,
                      appContent,
                      context: context || {},
                    }),
                  );
                });
              }
            },
            (err: unknown): express.Response => {
              return res
                .status(500)
                .send({ code: 500, messsage: err || 'Internal Server Error' });
            },
          );
        } else {
          if (isDevSSR) {
            const htmlService: HtmlService = new HtmlService();
            htmlService.getHTML().subscribe((html: string) => {
              const appContent: string | null = renderToString(
                <AppServer location={location} context={context} />,
              );
              return res.status(currentRoute.statusCode || 200).end(
                serverTemplateOutlet({
                  data: html,
                  appContent,
                  context: context || {},
                }),
              );
            });
          } else {
            const pathToTemplate: string | null = path.resolve(
              process.cwd(),
              'dist/client/index.html',
            );

            fs.readFile(pathToTemplate, 'utf-8', (_error, html) => {
              if (_error) {
                if (+(_error as { message: string | null }).message === 404) {
                  res.status(404).send('Page not found | 404');
                } else {
                  res.status(500).send('Internal server error | 500');
                }
              }

              const appContent: string | null = renderToString(
                <AppServer location={location} context={context} />,
              );

              return res.status(currentRoute.statusCode || 200).end(
                serverTemplateOutlet({
                  data: html,
                  appContent,
                  context: context || {},
                }),
              );
            });
          }
        }
      },
      (err: Error | null): express.Response => {
        return res
          .status(500)
          .send({ code: 500, messsage: err || 'Internal Server Error' });
      },
    );
  });
};

export default server;
