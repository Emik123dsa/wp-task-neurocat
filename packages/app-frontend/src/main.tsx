/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
import React from 'react';
import { hydrate, render } from 'react-dom';
import App from './app/app.module';
import { EventSourceBootstrap } from '@/shared/helpers/event.source.helper';
import { ServerInitData } from '@/core/models/server.model';
const isDevSSR: boolean = process.env.NODE_ENV === 'development';
const ɵroot: HTMLElement = document.getElementById('root');
export const ɵBootrstrap: () => void = (): void =>
  window.__SERVER_SIDE_RENDERED__
    ? hydrate(<App />, ɵroot)
    : render(<App />, ɵroot);

ɵBootrstrap();
// eslint-disable-next-line @typescript-eslint/no-explicit-any
declare let module: { hot: any };

if (window.__SERVER_SIDE_RENDERED__ && isDevSSR) {
  const $eventSourceStream: EventSourceBootstrap = new EventSourceBootstrap();
  $eventSourceStream.init();
}

if (module.hot) {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
  module.hot.accept('./app/app.module', () => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const App$ = require('./app/app.module').default;
    return window.__SERVER_SIDE_RENDERED__
      ? hydrate(<App$ />, ɵroot)
      : render(<App$ />, ɵroot);
  });
}

declare global {
  export interface Window {
    __LOCALE__: string;
    __STYLES__: string;
    __SERVER_SIDE_RENDERED__: boolean;
    __INITIAL_DATA__: ServerInitData;
  }
}
