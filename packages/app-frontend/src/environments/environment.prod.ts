import { ɵenv } from '@/core/models/env.model';

export const environment: ɵenv = {
  production: true,
  hmr: false,
  ɵApiSchemaUrl: new URL('http://localhost'),
  ɵExternalWS: null,
};
