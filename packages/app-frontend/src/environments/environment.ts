import { ɵenv } from '@/core/models/env.model';

export const environment: ɵenv = {
  production: false,
  hmr: true,
  ɵApiSchemaUrl: new URL('http://kubernetes.docker.internal/wp-json/react/v2/'),
  ɵExternalWS: new URL('http://localhost:4201'),
};
