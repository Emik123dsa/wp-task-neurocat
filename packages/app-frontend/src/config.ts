import { environment } from './environments/environment';
/**
 * Config implementation
 * according to the env
 *
 * @export
 * @class Config
 */
export class Config {
  public static readonly production: boolean = environment.production;
  public static readonly hmr: boolean = environment.hmr;
  public static readonly ɵApiSchemaUrl: URL | string | null =
    environment.ɵApiSchemaUrl;
  public static readonly ɵExternalWS: URL | string | null =
    environment.ɵExternalWS;
}
