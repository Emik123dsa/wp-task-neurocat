import React from 'react';
import { render } from '@testing-library/react';
import { ContactComponent } from './contact.component';

describe('Contact', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<ContactComponent />);
    expect(baseElement).toBeTruthy();
  });

  // it('should have a greeting as the title', () => {
  //   const { getByText } = render(<HomeComponent />);
  //   expect(getByText('Welcome to hello!')).toBeTruthy();
  // });
});
