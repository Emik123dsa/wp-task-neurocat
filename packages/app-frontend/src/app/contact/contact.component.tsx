import React, { Fragment } from 'react';
import { ContactFormComponent } from './contact-form/contact-form.component';

export const ContactComponent: React.FunctionComponent<unknown> = () => (
  <Fragment>
    <ContactFormComponent />
  </Fragment>
);
