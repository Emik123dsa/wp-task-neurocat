/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import React from 'react';
import {
  ContactFormImpl,
  ContactMultiFormEnum,
  ContactMultiFormImpl,
} from '@/core/models/form.model';
import {
  ContactFormValidationSchemaImpl,
  contactFormValidationSchema,
} from '@/core/validations/form.schema';
import { fromEvent, ReplaySubject, Subscription } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import Select, { OptionTypeBase, Props } from 'react-select';
import NoSSR from 'react-no-ssr';
import * as yup from 'yup';
import { InputComponent } from '@/shared/forms/input/input.component';

export const INTERESETS_OPTIONS_FACTORY: ReadonlyArray<
  {
    value: string;
    label: string;
  } & OptionTypeBase
> = [
  { value: 'Reading', label: 'Reading' },
  { value: 'Driving', label: 'Driving' },
  { value: 'Programming', label: 'Programming' },
  { value: "Making Neurocat's Tasks", label: "Making Neurocat's Tasks" },
];

export const MODELS_OPTIONS_FACTORY: ReadonlyArray<
  {
    value: string;
    label: string;
  } & OptionTypeBase
> = [
  { value: 'Model 1', label: 'Model 1' },
  { value: 'Model 2', label: 'Model 2' },
  { value: 'Model 3', label: 'Model 3' },
  { value: 'Model 4', label: 'Model 4' },
];

export const FREQUENCES_OPTIONS_FACTORY: ReadonlyArray<
  {
    value: string;
    label: string;
  } & OptionTypeBase
> = [
  { value: 'Often', label: 'Often' },
  { value: 'Seldom', label: 'Seldom' },
  { value: 'Never', label: 'Never' },
];

export class ContactFormComponent extends React.Component<
  unknown,
  ContactFormValidationSchemaImpl
> {
  /**
   * Form Reference
   *
   * @private
   * @type {React.RefObject<HTMLFormElement>}
   * @memberof ContactFormComponent
   */
  private _formRef: React.RefObject<HTMLFormElement>;
  /**
   * Form action on the submit
   *
   * @private
   * @type {Subscription}
   * @memberof ContactFormComponent
   */
  private _formSubmit: Subscription;
  /**
   * Defintely unsubscribe
   * from other subscribers via helper
   *
   * @private
   * @type {ReplaySubject<void>}
   * @memberof ContactFormComponent
   */
  private _formSubject: ReplaySubject<void> = new ReplaySubject<void>(0);

  public constructor(props: unknown) {
    super(props);
    this._formRef = React.createRef<HTMLFormElement>();
    this.state = {
      name: '',
      email: '',
      phone: '',
      role: '',
      interest: '',
      models: [],
      frequences: [],
    };
  }

  public componentDidMount(): void {
    this._formSubmit = fromEvent(this._formRef.current, 'submit')
      .pipe(
        tap(($event: Event) => {
          $event.preventDefault();
          $event.stopImmediatePropagation();
        }),
        takeUntil(this._formSubject),
      )
      .subscribe((): void => {
        try {
          alert(JSON.stringify(this.state));
        } catch (e) {
          if (e instanceof SyntaxError) {
            alert('Something went wrong!');
          }
        }
      });
  }

  public componentWillUnmount(): void {
    if (this._formSubmit) {
      this._formSubject.next(null);
      this._formSubject.complete();
      this._formSubmit.unsubscribe();
    }
  }

  public render(): JSX.Element {
    return (
      <section className="contact-form pt-20">
        <div className="container px-6 mx-auto">
          <h1 className="text-white font-titillium-regular font-semibold text-2xl ">
            Contact Form
          </h1>
          <div className="grid grid-cols-12 relative z-50">
            <form
              className="col-span-6"
              ref={this._formRef}
              name="contactForm"
              contentEditable={false}
            >
              <div className="mt-4 col-span-6">
                <label htmlFor="name">
                  <InputComponent
                    type="text"
                    id="name"
                    name="name"
                    placeholder="Name"
                    onDispatch={(
                      $event: React.ChangeEvent<HTMLInputElement>,
                    ): void => this._handleFormInputCredentials($event)}
                  />

                  {this.schemaErrors('name') && (
                    <div className="text-error mt-2">
                      {this.schemaErrors('name')?.map(
                        (item: string, index: number) => (
                          <div
                            className="text-error pt-2 text-tiny font-thin font-heebo-regular"
                            key={index}
                          >
                            {item}
                          </div>
                        ),
                      )}
                    </div>
                  )}
                </label>
              </div>
              <div className="mt-4">
                <label htmlFor="email">
                  <InputComponent
                    type="text"
                    id="email"
                    name="email"
                    placeholder="E-mail"
                    onDispatch={(
                      $event: React.ChangeEvent<HTMLInputElement>,
                    ): void => this._handleFormInputCredentials($event)}
                  />
                  {this.schemaErrors('email') && (
                    <div className="pt-0">
                      {this.schemaErrors('email')?.map(
                        (item: string, index: number) => (
                          <div
                            className="text-error pt-2 text-tiny font-thin font-heebo-regular"
                            key={index}
                          >
                            {item}
                          </div>
                        ),
                      )}
                    </div>
                  )}
                </label>
              </div>

              <div className="mt-4">
                <label htmlFor="role">
                  <InputComponent
                    type="text"
                    id="role"
                    name="role"
                    placeholder="Role"
                    onDispatch={(
                      $event: React.ChangeEvent<HTMLInputElement>,
                    ): void => this._handleFormInputCredentials($event)}
                  />

                  {this.schemaErrors('role') && (
                    <div className="text-error pt-0">
                      {this.schemaErrors('role')?.map(
                        (item: string, index: number) => (
                          <div
                            className="text-error pt-2 text-tiny font-thin font-heebo-regular"
                            key={index}
                          >
                            {item}
                          </div>
                        ),
                      )}
                    </div>
                  )}
                </label>
              </div>
              <div className="mt-4">
                <label htmlFor="phone">
                  <InputComponent
                    type="text"
                    id="phone"
                    name="phone"
                    placeholder="Phone"
                    onDispatch={(
                      $event: React.ChangeEvent<HTMLInputElement>,
                    ): void => this._handleFormInputCredentials($event)}
                  />

                  {this.schemaErrors('phone') && (
                    <div className="pt-0">
                      {this.schemaErrors('phone')?.map(
                        (item: string, index: number) => (
                          <div
                            className="text-error pt-2 font-thin font-heebo-regular"
                            key={index}
                          >
                            {item}
                          </div>
                        ),
                      )}
                    </div>
                  )}
                </label>
              </div>
              <NoSSR>
                <div className="mt-4">
                  <label htmlFor="interest">
                    <Select
                      id="interest"
                      name="interest"
                      className="font-heebo-regular font-thin text-dark"
                      placeholder="Interest"
                      onChange={($event: Props<OptionTypeBase>): void =>
                        this._handleFormSelectCredentials({
                          payload: $event,
                          name: ContactMultiFormEnum.INTEREST,
                          isMultiply: false,
                        })
                      }
                      options={INTERESETS_OPTIONS_FACTORY}
                      classNamePrefix="select"
                    ></Select>
                    {this.schemaErrors('interest') && (
                      <div className="text-error mt-2">
                        {this.schemaErrors('interest')?.map(
                          (item: string, index: number) => (
                            <div
                              className="text-error pt-2 text-tiny font-thin font-heebo-regular"
                              key={index}
                            >
                              {item}
                            </div>
                          ),
                        )}
                      </div>
                    )}
                  </label>
                </div>

                <div className="mt-4">
                  <label htmlFor="models">
                    <Select
                      id="models"
                      name="models"
                      isMulti
                      placeholder="How are you currently testing your models?"
                      onChange={($event: Props<OptionTypeBase>): void =>
                        this._handleFormSelectCredentials({
                          payload: $event,
                          name: ContactMultiFormEnum.MODELS,
                          isMultiply: true,
                        })
                      }
                      isDisabled={this._isMultiplySelectEnabled}
                      options={MODELS_OPTIONS_FACTORY}
                      className="font-thin"
                      classNamePrefix="select"
                    ></Select>
                    {this.schemaErrors('models') && (
                      <div className="text-error pt-0">
                        {this.schemaErrors('models')?.map(
                          (item: string, index: number) => (
                            <div
                              className="text-error pt-2 font-thin font-heebo-regular"
                              key={index}
                            >
                              {item}
                            </div>
                          ),
                        )}
                      </div>
                    )}
                  </label>
                </div>
                <div className="mt-4">
                  <label htmlFor="frequences">
                    <Select
                      isMulti
                      id="frequences"
                      name="frequences"
                      placeholder="How often do you test your models?"
                      onChange={($event: Props<OptionTypeBase>): void =>
                        this._handleFormSelectCredentials({
                          payload: $event,
                          name: ContactMultiFormEnum.FREQUENCES,
                          isMultiply: true,
                        })
                      }
                      isDisabled={this._isMultiplySelectEnabled}
                      options={FREQUENCES_OPTIONS_FACTORY}
                      className="font-thin "
                      classNamePrefix="select"
                    ></Select>
                    {this.schemaErrors('frequences') && (
                      <div className="text-error pt-0">
                        {this.schemaErrors('frequences')?.map(
                          (item: string, index: number) => (
                            <div
                              className="text-error pt-2 font-thin font-heebo-regular"
                              key={index}
                            >
                              {item}
                            </div>
                          ),
                        )}
                      </div>
                    )}
                  </label>
                </div>
              </NoSSR>
              <div className="mt-8">
                <button
                  disabled={!this.isValidContactForm}
                  className="button button-primary"
                  role="button"
                >
                  Submit
                </button>
              </div>
            </form>
          </div>
        </div>
      </section>
    );
  }

  private _handleFormInputCredentials(
    $event: React.ChangeEvent<HTMLInputElement>,
  ): void {
    $event.persist();
    $event.preventDefault();
    $event.stopPropagation();
    this.setState((prevState: ContactFormImpl) => ({
      ...prevState,
      [$event.target.name as keyof ContactFormImpl]: $event.target.value,
    }));
  }

  private _handleFormSelectCredentials($event: {
    name: ContactMultiFormImpl;
    isMultiply: boolean;
    payload: Props<OptionTypeBase>;
  }): void {
    this.setState((prevState: ContactFormImpl) => ({
      ...prevState,
      [$event?.name as keyof ContactFormImpl]: $event.isMultiply
        ? ($event?.payload as Props<OptionTypeBase>[]).map(
            (item: Props<OptionTypeBase>) => item.value,
          )
        : $event?.payload?.value,
    }));
  }

  public get isValidContactForm(): Readonly<boolean> {
    return contactFormValidationSchema.isValidSync(this.state);
  }

  private get _validationErrorsForm(): yup.ValidationError[] {
    try {
      return contactFormValidationSchema.validateSync(this.state, {
        abortEarly: false,
      }) as yup.ValidationError[];
    } catch (validationError) {
      return (validationError as yup.ValidationError)?.inner;
    }
  }

  public get schemaErrors(): (
    option: keyof ContactFormValidationSchemaImpl,
  ) => string[] {
    return (option: keyof ContactFormValidationSchemaImpl): string[] => {
      return Array.isArray(this._validationErrorsForm)
        ? this._validationErrorsForm?.find(item => item.path === option)?.errors
        : [];
    };
  }

  private get _isMultiplySelectEnabled(): Readonly<boolean> {
    let formControlValues: (string | number | symbol)[] = Reflect.ownKeys(
      this.state,
    );
    formControlValues = formControlValues.filter(
      (value: string) =>
        value !== ContactMultiFormEnum.FREQUENCES &&
        value !== ContactMultiFormEnum.MODELS,
    );
    return formControlValues.some(
      (value: keyof ContactFormImpl) => !this.state[value],
    );
  }
}
