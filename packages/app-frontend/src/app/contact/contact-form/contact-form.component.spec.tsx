import React from 'react';
import { render } from '@testing-library/react';
import { ContactFormComponent } from './contact-form.component';

describe('Contact Form Component', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<ContactFormComponent />);
    expect(baseElement).toBeTruthy();
  });

  // it('should have a greeting as the title', () => {
  //   const { getByText } = render(<HomeComponent />);
  //   expect(getByText('Welcome to hello!')).toBeTruthy();
  // });
});
