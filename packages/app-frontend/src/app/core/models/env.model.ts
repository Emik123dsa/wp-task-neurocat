export interface ɵenv {
  production: boolean;
  hmr: boolean;
  ɵApiSchemaUrl: URL | string | null;
  ɵExternalWS?: URL | string | null;
}
