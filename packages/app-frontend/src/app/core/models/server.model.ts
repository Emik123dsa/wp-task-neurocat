import React from 'react';
import * as express from 'express';
import { Url } from 'node:url';
import { Observable } from 'rxjs/internal/Observable';
import { NavigationHeaderItemImpl } from './navbar-items.model';
import { StaticContextInitialDataImpl } from '@/core/models/request.initial.data.model';
import { CategoryItemImpl } from '@/core/models/category.model';
/**
 * Server Props for
 * data server rendering
 *
 * @export
 * @interface IServerProps
 */
export interface IServerProps<T extends unknown> {
  children?: React.ReactNode;
  context?: T;
  location?: Url;
}
/**
 * Template outler server
 *
 * @export
 * @interface ServerTemplateOutletImpl
 */
export interface ServerTemplateOutletImpl {
  data: string | null;
  appContent: string | null;
  context: IServerProps<unknown>;
}
/**
 * Server Status Code
 *
 * @export
 * @interface ServerStatusCodeImpl
 */
export interface ServerStatusCodeImpl {
  statusCode?: number;
}
/**
 * Server on
 * init implementation
 *
 * @export
 * @interface ServerInitImpl
 */
export interface ServerInitImpl<T> {
  onServerInit: (
    req?: express.Request,
    res?: express.Response,
    next?: express.NextFunction,
  ) => Observable<T>;
}
/**
 * Server init Data
 *
 * @export
 * @interface ServerInitData
 * @template T
 */
export interface ServerInitialDataImpl {
  navigationHeaders?: NavigationHeaderItemImpl[];
  appContent?: CategoryItemImpl[];
}

export type ServerInitData = ServerInitialDataImpl &
  StaticContextInitialDataImpl &
  IServerProps<unknown>;
