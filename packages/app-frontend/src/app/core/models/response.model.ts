/**
 * Response entity schematics
 *
 * @export
 * @interface ɵResponseEntity
 * @template T
 */
export interface ɵResponseEntity<T extends unknown = unknown> {
  status: number | null;
  data: T;
  message: string | null;
}
