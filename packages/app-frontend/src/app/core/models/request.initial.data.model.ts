import { Observable } from 'rxjs/internal/Observable';
import React from 'react';
import { Url } from 'node:url';
/**
 * Request initial
 * to fetch some data
 * from server
 *
 * @export
 * @interface IRequestInitialData
 */
export interface IRequestInitialData {
  requestInitialData: <T extends any>() => Observable<T>;
}
/**
 * Static context as props
 *
 * @export
 * @interface StaticContext
 * @template T
 */
export interface StaticContext<T> {
  staticContext: T;
}

export type TAction = 'REPLACE';
/**
 * Static Context Initial Data
 *
 * @export
 * @interface StaticContextInitialData
 */
export interface StaticContextInitialDataImpl {
  action: TAction;
  location: Url;
  url: string | null;
}

export type RequestInitialDataImpl = IRequestInitialData &
  React.ComponentType<unknown>;
