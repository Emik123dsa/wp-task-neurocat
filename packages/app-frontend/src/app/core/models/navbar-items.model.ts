/**
 * Header Impl
 *
 * @export
 * @interface HeaderItems
 */
export interface NavigationHeaderItemImpl {
  ID: number;
  title: string;
  name: string;
}
/**
 * Footer items
 *
 * @export
 * @enum {number}
 */
export enum EFooterItems {
  ADDRESS = 'Address',
  PHONE = 'Phone',
  EMAIL = 'Email',
}
