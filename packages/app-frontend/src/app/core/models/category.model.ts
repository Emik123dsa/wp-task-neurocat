export type PostStatus = 'publish' | 'draft' | 'pending' | null;
/**
 * Post item impl
 *
 * @export
 * @interface PostItemImpl
 */
export interface PostItemImpl {
  postTitle: string | null;
  postStatus: PostStatus;
  postContent: string;
  postDescription: string | null;
}
/**
 * Category item impl
 *
 * @export
 * @interface CategoryItemImpl
 */
export interface CategoryItemImpl {
  categoryId: number | null;
  categoryName: string;
  categoryParent?: number;
  categoryDescription?: string;
  categoryChildren?: Omit<CategoryItemImpl, 'categoryChildren'>[];
  categoryDetails?: PostItemImpl[];
}
/**
 * Post feature icons
 * schematics
 *
 * @export
 * @enum {number}
 */
export enum EPostFeatureIcon {
  MISSION = 'mission',
  VISION = 'vision',
}
