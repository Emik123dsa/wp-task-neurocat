import { Observable } from 'rxjs';

/**
 * Fetch Impl
 *
 * @export
 * @interface FetchImpl
 */
export interface FetchImpl {
  get: ($url: string | null) => Observable<any>;
  post: ($url: string | null) => Observable<any>;
}
