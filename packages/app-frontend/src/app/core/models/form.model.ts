/**
 * Contact Form Impl
 *
 * @export
 * @interface IContactForm
 */
export interface IContactForm {
  name: string;
  email: string;
  phone: string;
  role: string | null;
  interest: string;
  models: Array<string>;
  frequences: Array<string>;
}

export type ContactMultiFormImpl = 'interest' | 'models' | 'frequences';

export enum ContactMultiFormEnum {
  INTEREST = 'interest',
  MODELS = 'models',
  FREQUENCES = 'frequences',
}

export type ContactFormImpl = { [P in keyof IContactForm]?: IContactForm[P] };
