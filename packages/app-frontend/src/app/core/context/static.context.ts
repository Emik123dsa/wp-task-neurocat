import React from 'react';
import { ServerInitData } from '@/core/models/server.model';

const STATIC_CONTEXT_FACTORY = (): ServerInitData => ({} as ServerInitData);
/**
 * Static Context factory,
 * we can use it the provider
 */
export const StaticAppContext: React.Context<ServerInitData> = React.createContext<ServerInitData>(
  STATIC_CONTEXT_FACTORY(),
);
