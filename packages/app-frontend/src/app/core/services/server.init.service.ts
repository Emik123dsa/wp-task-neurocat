import { ServerInitImpl, ServerInitData } from '@/core/models/server.model';
import { Observable } from 'rxjs/internal/Observable';
import { forkJoin } from 'rxjs';
import { LandingService } from '@/core/services/landing.service';
import { take } from 'rxjs/operators';
import { ServerInitialDataImpl } from '@/core/models/server.model';

export class ServerInit implements ServerInitImpl<ServerInitialDataImpl> {
  protected static _instance: ServerInit = null;
  /**
   * Singleton server init
   * @memberof ServerInit
   */
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  private constructor() {}
  /**
   * Get current instance
   * schematics,
   * we need to use in one time before
   * server will be outputed response
   *
   * @static
   * @returns {ServerInit}
   * @memberof ServerInit
   */
  public static getInstance(): ServerInit {
    if (!ServerInit._instance) {
      ServerInit._instance = new ServerInit();
    }
    return ServerInit._instance;
  }
  /**
   * On Server init is allowing us
   * to prefetch some data before
   * component will be rendered
   *
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} [next]
   * @returns {Observable<ServerInitData>}
   * @memberof ServerInit
   */
  public onServerInit(): Observable<ServerInitialDataImpl> {
    const landingService: LandingService = new LandingService();
    return forkJoin({
      navigationHeaders: landingService.getHeaderContent(),
      appContent: landingService.getAppContent(),
    }).pipe(take(1));
  }
}
