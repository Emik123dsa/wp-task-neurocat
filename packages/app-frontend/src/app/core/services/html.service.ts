import { FetchService } from './fetch.service';
import { Observable } from 'rxjs/internal/Observable';
import { Config } from '~/config';
import { exhaustMap, shareReplay } from 'rxjs/operators';

export class HtmlService extends FetchService {
  public getHTML(): Observable<string> {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    const { href }: URL = Config.ɵExternalWS as URL;
    return this.get(href).pipe(
      exhaustMap((res: Response) => res.text()),
      shareReplay(1),
    );
  }
}
