/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */

import { Observable } from 'rxjs/internal/Observable';
import { Config } from '~/config';
import { asyncScheduler, of } from 'rxjs';
import { catchError, observeOn } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators';
import { FetchService } from './fetch.service';
import { mergeMap } from 'rxjs/operators';
import { Observer } from 'rxjs';

export class ApiService extends FetchService {
  /**
   * Api service,
   * we can fetch get query
   *
   * @returns {Observable<unknown>}
   * @memberof ApiService
   */
  public $get<T extends unknown = Record<string, unknown>>(
    $url: string | null,
  ): Observable<unknown> {
    const { href }: URL = Config.ɵApiSchemaUrl as URL;
    const ɵRequest: string =
      $url.indexOf($url) === -1 ? $url : `${href}${$url}`;
    return this.get(ɵRequest).pipe(
      mergeMap(($data: Response) =>
        new Observable((observer: Observer<T>): void => {
          if (!$data.ok) return observer.error($data);
          $data
            .json()
            .then(($payload: T): void => {
              observer.next($payload);
              observer.complete();
            })
            .catch((err: Error | null): void => {
              observer.error(err);
            });
        }).pipe(observeOn(asyncScheduler)),
      ),
      catchError(this._formatErrors),
    );
  }
  /**
   * Post request,
   * we can fetch and submit post query
   *
   * @returns {Observable<unknown>}
   * @memberof ApiService
   */
  public $post($url: string | null): Observable<unknown> {
    const { href }: URL = Config.ɵApiSchemaUrl as URL;
    const ɵRequest: string =
      $url.indexOf($url) === -1 ? $url : `${href}${$url}`;
    return this.post(ɵRequest).pipe(
      switchMap((res: Response) => of(res.json())),
      catchError(this._formatErrors),
    );
  }

  protected _isValidRequest($status: number): Readonly<boolean> {
    return $status === 200;
  }
}
