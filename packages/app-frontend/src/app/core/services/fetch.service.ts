/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/unbound-method */
import { switchMap, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';
import { throwError } from 'rxjs/internal/observable/throwError';
import { FetchMethods } from '@/core/enums/methods.enum';
import { Observer, of } from 'rxjs';
import { FetchAdapter } from '@/core/adapters/fetch.adapter';
import { FetchImpl } from '@/core/models/fetch.model';

export class FetchService extends FetchAdapter implements FetchImpl {
  public get($url: string | null): Observable<any> {
    return new Observable<any>((observer: Observer<Response>) => {
      this._fetch($url, {
        method: FetchMethods.GET,
      })
        .then((res: Response) => {
          observer.next(res);
          observer.complete();
        })
        .catch(err => {
          observer.error(err);
        });
    }).pipe(
      switchMap((res: Response) => {
        if (!res.ok) return throwError(res);
        return of(res);
      }),
      catchError(this._formatErrors),
    );
  }

  public post($url: string | null): Observable<any> {
    return new Observable<any>((observer: Observer<Response>) => {
      this._fetch($url, {
        method: FetchMethods.POST,
      })
        .then((res: Response) => {
          observer.next(res);
          observer.complete();
        })
        .catch(err => {
          observer.error(err);
        });
    }).pipe(
      switchMap((res: Response) => {
        if (!res.ok) {
          return throwError(res);
        }
        return of(res);
      }),
      catchError(this._formatErrors),
    );
  }

  protected _formatErrors(
    err: Error | null,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    _caught?: Observable<any>,
  ): Observable<never> {
    return throwError(err);
  }
}
