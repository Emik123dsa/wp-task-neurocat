import { ApiService } from '@/core/services/api.service';
import { of } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { switchMap } from 'rxjs/operators';
import { ɵResponseEntity } from '@/core/models/response.model';
import { NavigationHeaderItemImpl } from '@/core/models/navbar-items.model';
import { CategoryItemImpl } from '@/core/models/category.model';

export class LandingService extends ApiService {
  /**
   * Fetching header content

   *
   * @template NavigationHeaderItemImpl
   * @returns {Observable<
   *     NavigationHeaderItemImpl[]
   *   >}
   * @memberof LandingService
   */
  public getHeaderContent(): Observable<NavigationHeaderItemImpl[]> {
    return this.$get<ɵResponseEntity<NavigationHeaderItemImpl[]>>(
      'getHeaderContent',
    ).pipe(
      switchMap(
        ({ status, data }: ɵResponseEntity<NavigationHeaderItemImpl[]>) => {
          return this._isValidRequest(status)
            ? of(data)
            : of([] as NavigationHeaderItemImpl[]);
        },
      ),
    );
  }
  /**
   * Fetch app content entity,
   * we will inject it inside of home page
   *
   * @template CategoryItemImpl
   * @returns {Observable<CategoryItemImpl[]>}
   * @memberof LandingService
   */
  public getAppContent(): Observable<CategoryItemImpl[]> {
    return this.$get('getAppContent').pipe(
      switchMap(({ status, data }: ɵResponseEntity<CategoryItemImpl[]>) => {
        return this._isValidRequest(status)
          ? of(data)
          : of([] as CategoryItemImpl[]);
      }),
    );
  }
}
