/**
 * Fetch Methods
 *
 * @export
 * @enum {number}
 */
export enum FetchMethods {
  GET = 'GET',
  POST = 'POST',
  DELETE = 'DELETE',
  PUT = 'PUT',
}
