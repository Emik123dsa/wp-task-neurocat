/* eslint-disable @typescript-eslint/no-explicit-any */
import { Observable } from 'rxjs';
import fetch from 'isomorphic-fetch';
export abstract class FetchAdapter {
  protected _fetch: typeof fetch;
  public constructor() {
    this._fetch = fetch;
  }
  public abstract get($url: string | null): Observable<any>;
  public abstract post($url: string | null): Observable<any>;
}
