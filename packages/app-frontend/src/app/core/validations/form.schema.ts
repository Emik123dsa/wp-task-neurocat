import * as yup from 'yup';

export const contactFormValidationSchema: yup.AnyObjectSchema = yup.object({
  name: yup.string().required('* Name is required'),
  email: yup
    .string()
    .email('* E-mail is not valid format')
    .required('* E-mail is required'),
  phone: yup.string().notRequired(),
  role: yup.string().required('* Role is required'),
  interest: yup.string().required('* Interest is required'),
  models: yup.array().of(yup.string().nullable()),
  frequences: yup.array().of(yup.string().nullable()),
});

export type ContactFormValidationSchemaImpl = yup.InferType<
  typeof contactFormValidationSchema
>;
