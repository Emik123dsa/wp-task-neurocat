import React from 'react';
export type InputType = 'email' | 'text' | 'number';
/**
 * Input Props Impl
 *
 * @export
 * @interface InputPropsImpl
 */
export interface InputPropsImpl {
  type: InputType;
  id: string;
  name: string;
  placeholder: string;
  onDispatch?: ($event: React.ChangeEvent<HTMLInputElement>) => void;
}

export const INPUT_PROPS_FACTORY: () => InputPropsImpl = () => ({
  type: 'text',
  id: 'default',
  placeholder: 'default',
  name: 'default',
});

export class InputComponent extends React.Component<InputPropsImpl> {
  static defaultProps: InputPropsImpl = INPUT_PROPS_FACTORY();
  public render(): JSX.Element {
    const { type, id, name, placeholder }: Partial<InputPropsImpl> = this.props;
    return (
      <input
        type={type}
        id={id}
        name={name}
        style={{ borderWidth: '1px' }}
        placeholder={placeholder}
        autoComplete={'off'}
        className="px-3 py-2 rounded-md w-full outline-none border-dark hover:border-blue"
        onInput={($event: React.ChangeEvent<HTMLInputElement>): void => {
          if (this.props.onDispatch) {
            this.props.onDispatch($event);
          }
        }}
      />
    );
  }
}
