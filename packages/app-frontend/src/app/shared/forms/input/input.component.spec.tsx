import React from 'react';
import { render } from '@testing-library/react';
import { InputComponent } from './input.component';

describe('Input', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <InputComponent placeholder="jest" type="text" id="jest" name="jest" />,
    );
    expect(baseElement).toBeTruthy();
  });

  // it('should have a greeting as the title', () => {
  //   const { getByText } = render(<HomeComponent />);

  //   expect(getByText('Welcome to hello!')).toBeTruthy();
  // });
});
