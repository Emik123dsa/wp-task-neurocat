import { Location } from 'history';
import React from 'react';

import { Link } from 'react-router-dom';
// import NavbarLogo from '~/assets/img/neurocat_website_logo.png';
const NavbarLogo: string | null =
  'https://www.neurocat.ai/wp-content/uploads/2018/11/neurocat_website_logo.png';

export const LogoComponent: React.StatelessComponent = (): JSX.Element => {
  return (
    <div className="w-auto cursor-pointer">
      <Link
        to={(location: Location<unknown>) => ({
          ...location,
          pathname: '/home',
        })}
      >
        <img
          className="bg-contain bg-clip-border bg-no-repeat pointer-events-none select-none"
          src={NavbarLogo}
          srcSet={NavbarLogo}
          title="navbarLogo"
          alt="navbarLogo"
        />
      </Link>
    </div>
  );
};
