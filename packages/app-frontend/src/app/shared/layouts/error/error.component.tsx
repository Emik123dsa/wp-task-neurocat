import React, { Component } from 'react';
export class ErrorComponent extends Component<unknown, unknown> {
  public render(): JSX.Element {
    return (
      <div className="container px-6 mx-auto pt-20">
        <h1 className="text-white font-titillium-regular font-semibold text-2xl mb-16 pb-96">
          Error | 404
        </h1>
      </div>
    );
  }
}
