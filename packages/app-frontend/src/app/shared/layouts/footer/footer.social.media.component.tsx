import React from 'react';

import FacebookFooter from '~/assets/img/facebook.footer.svg';
import TwitterFooter from '~/assets/img/twitter.footer.svg';
import GoogleFooter from '~/assets/img/google.footer.svg';

export const FooterSocialMedia: React.FunctionComponent = (): JSX.Element => (
  <nav className="mt-6">
    <ul role="list" className="flex items-center justify-start gap-x-12">
      <li role="listitem" className="cursor-pointer">
        <a href="#" target="_blank" className="max-w-max">
          <FacebookFooter />
        </a>
      </li>
      <li role="listitem" className="cursor-pointer">
        <a href="#" target="_blank" className="max-w-max">
          <TwitterFooter />
        </a>
      </li>
      <li role="listitem" className="cursor-pointer">
        <a href="#" target="_blank" className="max-w-max">
          <GoogleFooter />
        </a>
      </li>
    </ul>
  </nav>
);
