import React from 'react';
import { PostItemImpl } from '@/core/models/category.model';
import { EFooterItems } from '@/core/models/navbar-items.model';
/**
 * Footer navbar props impl
 *
 * @export
 * @interface FooterNavbarPropsImpl
 */
export interface FooterNavbarPropsImpl {
  footerItems: PostItemImpl[];
}

export const FooterNavbarComponent: React.FunctionComponent<FooterNavbarPropsImpl> = ({
  footerItems,
}: FooterNavbarPropsImpl): JSX.Element => {
  const getContentFooter: ($payload: PostItemImpl) => JSX.Element = (
    $payload: PostItemImpl,
  ): JSX.Element => {
    switch ($payload.postTitle) {
      case EFooterItems.ADDRESS:
        return <address className="not-italic">{$payload.postContent}</address>;
      case EFooterItems.EMAIL:
        return (
          <a
            className="max-w-max font-heebo-regular font-thin block"
            target="_self"
            href={`mailto:${$payload.postContent}`}
          >
            {$payload.postContent}
          </a>
        );
      case EFooterItems.PHONE:
        return (
          <a
            className="max-w-max font-heebo-regular font-thin block"
            target="_self"
            href={`tel:${$payload.postContent}`}
          >
            {$payload.postContent}
          </a>
        );
    }
  };

  return (
    <nav role="navigation">
      <ul className="flex items-baseline gap-x-12" role="list">
        {footerItems?.map((item: PostItemImpl, index: number) => (
          <li key={`${item.postTitle}${index}`} role="listitem">
            <div className="font-thin text-white">{item.postTitle}</div>
            <div className="max-w-xs font-heebo-regular font-thin mt-4">
              {getContentFooter(item)}
            </div>
          </li>
        ))}
      </ul>
    </nav>
  );
};
