import React from 'react';
import { render } from '@testing-library/react';
import { FooterSocialMedia } from './footer.social.media.component';

describe('Footer Social Media', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<FooterSocialMedia />);
    expect(baseElement).toBeTruthy();
  });
});
