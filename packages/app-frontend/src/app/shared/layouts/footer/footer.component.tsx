import React, { useContext } from 'react';

import FooterWave from '~/assets/img/footer.bottom.svg';
import FooterDots from '~/assets/img/footer.bottom.dots.svg';
import { FooterNavbarComponent } from './footer.navbar.component';
import { FooterSocialMedia } from './footer.social.media.component';
import { Link } from 'react-router-dom';
import { Location } from 'history';
import { StaticAppContext } from '@/core/context/static.context';
import { ServerInitData } from '@/core/models/server.model';
import { CategoryItemImpl, PostItemImpl } from '@/core/models/category.model';

export const FooterComponent: React.FunctionComponent = () => {
  const { appContent }: ServerInitData = useContext(StaticAppContext);

  const footerItems: PostItemImpl[] =
    appContent?.find((item: CategoryItemImpl) =>
      item.categoryName.startsWith('Footer'),
    ).categoryDetails || ([] as PostItemImpl[]);

  if (!footerItems) {
    return null;
  }

  return (
    <footer className="px-0 xl:px-width footer py-10">
      <div style={{ zIndex: -20 }} className="absolute bottom-0 right-0">
        <FooterDots className="absolute top-32 right-40" />
        <FooterWave className="right-0" />
      </div>
      <div className="container px-6 mx-auto">
        <div className="flex items-center justify-between">
          <div className="relative block">
            <FooterNavbarComponent footerItems={footerItems} />
            <FooterSocialMedia />
          </div>
          <div className="block">
            <Link
              className="button button-primary"
              to={(location: Location<unknown>) => ({
                ...location,
                pathname: '/contact',
              })}
            >
              Contact Us
            </Link>
          </div>
        </div>
      </div>
    </footer>
  );
};
