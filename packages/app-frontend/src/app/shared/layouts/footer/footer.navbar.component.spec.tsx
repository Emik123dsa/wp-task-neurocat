import React from 'react';
import { render } from '@testing-library/react';
import { FooterNavbarComponent } from './footer.navbar.component';
import { PostItemImpl } from '@/core/models/category.model';

describe('Footer Navbar', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <FooterNavbarComponent footerItems={[] as PostItemImpl[]} />,
    );
    expect(baseElement).toBeTruthy();
  });
});
