import React from 'react';

import { NavigationHeaderItemImpl } from '@/core/models/navbar-items.model';

export interface NavbarPropsImpl {
  items: NavigationHeaderItemImpl[];
}

import { NavLink } from 'react-router-dom';

export class HeaderNavbarComponent extends React.Component<NavbarPropsImpl> {
  public render(): JSX.Element {
    const { items }: Partial<NavbarPropsImpl> = this.props;

    return (
      <nav role="navigation">
        <ul
          className="flex flex-row gap-x-6 flex-shrink-0 flex-grow-0"
          role="menu"
        >
          {items
            ? items.map(({ ID, title, name }: NavigationHeaderItemImpl) => (
                <li
                  key={ID}
                  role="listitem"
                  className="font-heebo-light transition duration-200 text-white font-thin hover:text-blue"
                >
                  <NavLink to={`/${name}`}>{title}</NavLink>
                </li>
              ))
            : null}
        </ul>
      </nav>
    );
  }
}
