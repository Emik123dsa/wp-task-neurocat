import React, { Fragment, useContext } from 'react';

import HeaderTop from '~/assets/img/header.top.svg';
import { HeaderNavbarComponent } from './header.navbar.component';
import { LogoComponent } from '@/shared/layouts/logo/logo.component';
import { HeroReveal } from '@/shared/helpers/hero-reveal/hero-reveal.component';
import { StaticAppContext } from '@/core/context/static.context';
import { ServerInitData } from '@/core/models/server.model';

export const HeaderComponent: React.FunctionComponent = (): JSX.Element => {
  const { navigationHeaders }: ServerInitData = useContext(StaticAppContext);

  return (
    <Fragment>
      <div className="z-20 absolute right-0 top-0">
        <HeaderTop />
        <div className="absolute mt-12 top-1/2 transform ml-24 -translate-y-1/2">
          <HeroReveal />
        </div>
      </div>
      <header
        className="header px-0 xl:px-width pt-6 w-full"
        role="contentinfo"
      >
        <div className="container px-6 mx-auto relative">
          <div className="flex flex-row flex-shrink-0 justify-items-center items-center justify-between">
            <div className="header__logo">
              <LogoComponent />
            </div>
            <div className="header__navbar relative z-40">
              <HeaderNavbarComponent items={navigationHeaders} />
            </div>
          </div>
        </div>
      </header>
    </Fragment>
  );
};
