import React from 'react';

import RectangleMiddle from '~/assets/img/rectangle.middle.svg';
import RectangleLeftMiddle from '~/assets/img/rectangle.left.middle.svg';
import RectangleRightMiddle from '~/assets/img/rectangle.right.middle.svg';
import RectangleLeftBottom from '~/assets/img/rectangle.left.bottom.svg';
import RectangleRightBottom from '~/assets/img/rectangle.bottom.right.svg';
import RectangleTopLeftMiddle from '~/assets/img/rectangle.top.left.middle.svg';
import RectangleTopLeftHuge from '~/assets/img/rectangle.top.left.huge.svg';
import RectangleTopRight from '~/assets/img/rectangle.top.right.svg';
import DotsTopRight from '~/assets/img/dots.top.right.svg';
import DotsMiddle from '~/assets/img/dots.middle.svg';
import DotsBottomLeft from '~/assets/img/dots.bottom.left.svg';
import HeroBottomWave from '~/assets/img/hero.bottom.wave.svg';

export const HeroReveal: React.FunctionComponent = () => (
  <div className="hero-reveal">
    <div className="hero-reveal-monitor">
      <div className="absolute right-0 transform -translate-y-12">
        <RectangleTopRight />
      </div>
      <div className="absolute right-0 transform -translate-y-28 mr-2">
        <DotsTopRight />
      </div>
      <div className="hero-reveal-top absolute transform -translate-y-full top-0">
        <div className="transform translate-x-12">
          <RectangleTopLeftHuge />
        </div>
        <div className="transform translate-y-20 -translate-x-14">
          <RectangleTopLeftMiddle />
        </div>
      </div>
      <div className="hero-reveal-middle flex items-center justify-between">
        <div className="transform translate-x-1/2 mt-20 z-50">
          <RectangleLeftMiddle />
        </div>
        <div>
          <RectangleMiddle />
        </div>
        <div className="transform translate-x-12">
          <RectangleRightMiddle />
        </div>
      </div>
      <div className="absolute top-0 transform translate-y-14 translate-x-20 z-50">
        <DotsMiddle />
      </div>
      <div className="hero-reveal-bottom flex items-center justify-between">
        <div className="transform translate-x-20 translate-y-6">
          <RectangleLeftBottom />
        </div>
        <div className="transform translate-y-2 -translate-x-8">
          <RectangleRightBottom />
        </div>
      </div>
      <div className="absolute transform -translate-x-12 left-0">
        <DotsBottomLeft />
      </div>
      <div className="absolute transform -translate-y-20 translate-x-24">
        <HeroBottomWave />
      </div>
    </div>
  </div>
);
