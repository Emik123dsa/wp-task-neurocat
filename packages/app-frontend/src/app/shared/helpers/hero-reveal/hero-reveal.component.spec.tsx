import React from 'react';
import { render } from '@testing-library/react';
import { HeroReveal } from './hero-reveal.component';

describe('Feature Item Component', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<HeroReveal />);
    expect(baseElement).toBeTruthy();
  });

  // it('should have a greeting as the title', () => {
  //   const { getByText } = render(<HomeComponent />);

  //   expect(getByText('Welcome to hello!')).toBeTruthy();
  // });
});
