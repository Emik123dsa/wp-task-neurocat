import React from 'react';
import { PostItemImpl } from '@/core/models/category.model';
/**
 * Advantage item impl
 *
 * @export
 * @interface AdvantageItemImpl
 */
export interface AdvantageItemImpl {
  item: PostItemImpl;
}

export const AdvantageItemComponent: React.FunctionComponent<AdvantageItemImpl> = ({
  item,
}: AdvantageItemImpl): JSX.Element => (
  <li className=" flex flex-col items-center bg-item p-10 shadow-2xl rounded-xl cursor-pointer">
    <div className="mb-4 pointer-events-none select-none">
      <img
        className="bg-contain bg-no-repeat h-24 w-auto"
        src={item.postDescription || null}
        srcSet={item.postDescription || null}
      />
    </div>
    <div className="max-w-md font-thin text-md text-white mb-6">
      {item.postTitle}
    </div>
    <div className="max-w-md text-sm font-medium">{item.postContent}</div>
  </li>
);
