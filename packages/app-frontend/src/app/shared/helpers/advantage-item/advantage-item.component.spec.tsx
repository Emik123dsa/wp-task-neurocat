import React from 'react';
import { render } from '@testing-library/react';
import { AdvantageItemComponent } from './advantage-item.component';
import { PostItemImpl } from '@/core/models/category.model';

describe('Advantage Item Component', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <AdvantageItemComponent item={{} as PostItemImpl} />,
    );
    expect(baseElement).toBeTruthy();
  });

  // it('should have a greeting as the title', () => {
  //   const { getByText } = render(<HomeComponent />);

  //   expect(getByText('Welcome to hello!')).toBeTruthy();
  // });
});
