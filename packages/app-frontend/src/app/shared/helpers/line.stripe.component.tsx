import React from 'react';

export const LineStripe: React.FunctionComponent = () => (
  <div
    style={{ height: '1px', borderRadius: '100%' }}
    className="bg-gray opacity-50 w-full relative block"
  ></div>
);
