import React from 'react';
import { render } from '@testing-library/react';
import { FeatureItemComponent } from './feature-item.component';
import { PostItemImpl } from '@/core/models/category.model';

describe('Feature Item Component', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <FeatureItemComponent item={{} as PostItemImpl} />,
    );
    expect(baseElement).toBeTruthy();
  });

  // it('should have a greeting as the title', () => {
  //   const { getByText } = render(<HomeComponent />);

  //   expect(getByText('Welcome to hello!')).toBeTruthy();
  // });
});
