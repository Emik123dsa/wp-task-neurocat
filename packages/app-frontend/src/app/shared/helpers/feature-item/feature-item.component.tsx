import React from 'react';
import { EPostFeatureIcon, PostItemImpl } from '@/core/models/category.model';
import HeroMission from '~/assets/img/hero.mission.svg';
import HeroVision from '~/assets/img/hero.vision.svg';

/**
 * Feature items props
 *
 * @export
 * @interface IFeatureItemProps
 */
export interface IFeatureItemProps {
  item: PostItemImpl;
}

export const FeatureItemComponent: React.FunctionComponent<IFeatureItemProps> = ({
  item,
}: IFeatureItemProps): JSX.Element => (
  <li className="py-6" role="listitem">
    <div className="flex items-center flex-grow-0 flex-shrink-0">
      {item.postDescription !== EPostFeatureIcon.MISSION ? (
        <HeroVision />
      ) : (
        <HeroMission />
      )}
      <h3 className="ml-4 text-white font-titillium-regular text-base font-semibold">
        {item.postTitle}
      </h3>
    </div>
    <div className="mt-4">
      <p className="text-dark text-sm max-w-xl font-wp-ui-il font-medium">
        {item.postContent}
      </p>
    </div>
  </li>
);
