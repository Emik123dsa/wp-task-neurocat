/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import serialize from 'serialize-javascript';
import { ServerTemplateOutletImpl } from '@/core/models/server.model';
import { Helmet, HelmetData } from 'react-helmet';

export const serverTemplateOutlet: ({
  data,
  appContent,
  context,
}: ServerTemplateOutletImpl) => string = ({
  data,
  appContent,
  context,
}: ServerTemplateOutletImpl): string => {
  try {
    const helmet: HelmetData = Helmet.renderStatic();
    data = data.replace('<title></title>', `${helmet.title.toString()}`);
    data = data.replace(
      '<meta name="server-side-rendered" content="false">',
      helmet.meta.toString(),
    );

    data = data.replace(
      '<div id="root"></div>',
      `<div id="root" data-server-side-rendered="true">${appContent}</div>`,
    );

    data = data.replace(
      '<script data-server-side-rendered="false"></script>',
      '<script>window.__SERVER_SIDE_RENDERED__=true</script>',
    );

    data = data.replace(
      '<script data-initial-data="{}"></script>',
      `<script>window.__INITIAL_DATA__=${serialize(context)}</script>`,
    );
    return data;
  } catch (e) {
    return data;
  }
};
