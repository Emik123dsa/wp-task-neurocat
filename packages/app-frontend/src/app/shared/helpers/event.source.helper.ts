export class EventSourceBootstrap {
  private _eventSource: EventSource;

  private static readonly STREAM: string | null =
    'http://localhost:4200/event-stream';

  public constructor() {
    this._eventSource = new EventSource(EventSourceBootstrap.STREAM);
  }
  public init(): void {
    this._eventSource.addEventListener('message', ($event: never) => {
      console.log($event);
    });

    this._eventSource.addEventListener('open', ($event: never) => {
      console.log($event);
    });

    this._eventSource.addEventListener('error', ($event: never) => {
      console.log($event);
    });
  }
}
