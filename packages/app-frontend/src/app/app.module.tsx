/* eslint-disable react/no-children-prop */
import React, { Component } from 'react';
import { Router } from 'react-router-dom';
import {
  createBrowserHistory,
  createMemoryHistory,
  History,
  LocationState,
} from 'history';
import { ɵrootRoute } from '@/app.routing-module';
import { StaticAppContext } from '@/core/context/static.context';

import { ServerInitData } from '@/core/models/server.model';

const history: History<LocationState> = !window.__SERVER_SIDE_RENDERED__
  ? createMemoryHistory({
      initialEntries: ['/'],
    })
  : createBrowserHistory();

class App extends Component<unknown, ServerInitData> {
  public constructor(props: Readonly<unknown>) {
    super(props);
    if (window && typeof window === 'object')
      this.state = window?.__INITIAL_DATA__ ?? null;
  }

  public render(): JSX.Element {
    return (
      <StaticAppContext.Provider value={this.state}>
        <Router history={history}>{ɵrootRoute}</Router>
      </StaticAppContext.Provider>
    );
  }
}

export default App;
