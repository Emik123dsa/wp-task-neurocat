/* eslint-disable react/no-children-prop */
import * as React from 'react';
import { StaticRouter } from 'react-router-dom';
import { ɵrootRoute } from '@/app.routing-module';
import { IServerProps } from './core/models/server.model';
import { StaticAppContext } from './core/context/static.context';
import { StaticContextInitialDataImpl } from '@/core/models/request.initial.data.model';

class AppServer extends React.Component<IServerProps<unknown>, unknown> {
  public render(): JSX.Element {
    const { location, context }: IServerProps<unknown> = this.props;
    return (
      <StaticAppContext.Provider
        value={
          context as StaticContextInitialDataImpl & Record<string, unknown>
        }
      >
        <StaticRouter location={location} context={context}>
          {ɵrootRoute}
        </StaticRouter>
      </StaticAppContext.Provider>
    );
  }
}

export default AppServer;
