/* eslint-disable react/display-name */
import * as React from 'react';
import { Switch, Route, Redirect } from 'react-router';
import { RouteConfig } from 'react-router-config';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { AppComponent } from './app.component';
import { ErrorComponent } from './shared/layouts/error/error.component';
import { ServerStatusCodeImpl } from './core/models/server.model';

export const ɵroutes: (RouteConfig & ServerStatusCodeImpl)[] = [
  {
    path: '/home',
    exact: true,
    component: HomeComponent,
  },
  {
    path: '/contact',
    exact: true,
    component: ContactComponent,
  },
  {
    path: '*',
    exact: true,
    statusCode: 404,
    component: ErrorComponent,
  },
];

export const ɵrootRoute: JSX.Element = (
  <Switch>
    <Redirect exact from="/" to="/home" />
    <Route path="/" component={AppComponent} />
  </Switch>
);
