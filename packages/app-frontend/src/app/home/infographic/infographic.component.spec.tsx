import React from 'react';
import { render } from '@testing-library/react';
import { InfoGraphicComponent } from './infographic.component';

describe('Infographic', () => {
  it('it should be sucessfully rendered', () => {
    const { baseElement } = render(<InfoGraphicComponent />);
    expect(baseElement).toBeTruthy();
  });
});
