import React from 'react';
import { PostItemImpl } from '@/core/models/category.model';
/**
 * Infographic props impl
 *
 * @export
 * @interface InfographicPropsImpl
 */
export interface InfographicPropsImpl {
  titleFeatures: PostItemImpl[];
  essentialFeatures: PostItemImpl[];
  otherFeatures: PostItemImpl[];
}

export const InfoGraphicComponent: React.FunctionComponent<InfographicPropsImpl> = ({
  titleFeatures,
  essentialFeatures,
  otherFeatures,
}: InfographicPropsImpl) => (
  <section className="infographics py-20">
    <div className="container px-6 mx-auto">
      {titleFeatures?.map((item: PostItemImpl, index: number) => (
        <div key={`${item.postTitle}${index}`} className="mb-4">
          <h2 className="text-white mx-auto text-center font-titillium-regular font-semibold text-xl max-w-sm ">
            {item.postTitle}
          </h2>
        </div>
      ))}
      <div className="pb-20">
        <ul className="grid grid-cols-1 lg:grid-cols-3 grid-flow-row-dense gap-y-4">
          {essentialFeatures?.map((item: PostItemImpl, index: number) => (
            <li key={`${item.postTitle}${index}`} className="col-span-1">
              <div className="flex flex-col items-center">
                <div className="pointer-events-none select-none">
                  <img
                    className="bg-contain bg-no-repeat h-36 w-auto"
                    src={item.postDescription || null}
                    srcSet={item.postDescription || null}
                  />
                </div>
                <div className="max-w-md font-thin text-md text-white mb-4">
                  {item.postTitle}
                </div>
                <div className="max-w-xs text-xs text-center font-medium">
                  {item.postContent}
                </div>
              </div>
            </li>
          ))}
        </ul>
      </div>
      <div className="relative block">
        <div className="flex items-center flex-wrap justify-between relative z-50 h-full">
          {otherFeatures?.map((item: PostItemImpl, index: number) => (
            <div
              key={`${item.postTitle}${index}`}
              className="p-6 bg-item shadow-lg rounded-xl flex-shrink-0 h-full"
            >
              <div className="mb-6 pointer-events-none select-none">
                <img
                  className="h-12"
                  src={item.postDescription || null}
                  srcSet={item.postDescription || null}
                />
              </div>
              <p className="max-w-md text-xs font-medium">{item.postContent}</p>
            </div>
          ))}
        </div>
      </div>
    </div>
  </section>
);
