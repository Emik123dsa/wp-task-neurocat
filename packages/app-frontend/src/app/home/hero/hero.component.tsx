import { PostItemImpl } from '@/core/models/category.model';
import { FeatureItemComponent } from '@/shared/helpers/feature-item/feature-item.component';
import React from 'react';

export interface IHeroProps {
  verboseItems: PostItemImpl[];
  titleItems: PostItemImpl[];
}

export const HeroComponent: React.FunctionComponent<IHeroProps> = ({
  verboseItems,
  titleItems,
}: IHeroProps): JSX.Element => {
  return (
    <section className="hero pt-20 relative">
      <div className="container mx-auto px-6 relative z-50">
        {titleItems?.map((titleItem: PostItemImpl, index: number) => (
          <div key={`${titleItem.postTitle}${index}`}>
            <h1
              style={{ maxWidth: '16rem' }}
              className="text-white font-titillium-regular font-semibold text-2xl"
            >
              {titleItem.postTitle}
            </h1>
            <div className="mt-4 max-w-xl">
              <p className="font-heebo-regular text-base font-light">
                {titleItem.postContent}
              </p>
            </div>
          </div>
        ))}
        <div className="flex items-start justify-start">
          <ul className="mt-16" role="list">
            {verboseItems.map((verboseItem: PostItemImpl, index: number) => (
              <FeatureItemComponent
                key={`${verboseItem.postTitle}${index}`}
                item={verboseItem}
              />
            ))}
          </ul>
        </div>
      </div>
    </section>
  );
};
