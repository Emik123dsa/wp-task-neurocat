import React from 'react';
import { render } from '@testing-library/react';
import { HeroComponent } from './hero.component';
import { PostItemImpl } from '@/core/models/category.model';

describe('Hero', () => {
  it('it should be sucessfully rendered', () => {
    const itemsFactory: PostItemImpl[] = [];
    const { baseElement } = render(
      <HeroComponent verboseItems={itemsFactory} titleItems={itemsFactory} />,
    );
    expect(baseElement).toBeTruthy();
  });
});
