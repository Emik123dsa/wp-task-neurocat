import React from 'react';
import { render } from '@testing-library/react';
import { CustomerSwiperComponent } from './customer.swiper.component';
import { PostItemImpl } from '@/core/models/category.model';

describe('Customer Swiper', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <CustomerSwiperComponent items={[] as PostItemImpl[]} />,
    );
    expect(baseElement).toBeTruthy();
  });
});
