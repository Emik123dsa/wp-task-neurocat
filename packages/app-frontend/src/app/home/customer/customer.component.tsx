import React from 'react';
import { CustomerSwiperComponent } from './customer.swiper.component';
import { CategoryItemImpl } from '@/core/models/category.model';
/**
 * Customer Props Impl
 *
 * @export
 * @interface CustomerPropsImpl
 */
export interface CustomerPropsImpl {
  customerItems: CategoryItemImpl;
}

export const CustomerComponent: React.FunctionComponent<CustomerPropsImpl> = ({
  customerItems,
}: CustomerPropsImpl): JSX.Element => {
  return (
    <section className="customer py-20">
      <div className="container px-6 mx-auto">
        <div className="w-full">
          <h2 className="text-white mx-auto text-center font-titillium-regular font-semibold text-xl max-w-sm mb-4">
            {customerItems?.categoryName}
          </h2>
          <div className="customer-testimonials-swiper">
            <CustomerSwiperComponent
              items={customerItems?.categoryChildren[0]?.categoryDetails}
            />
          </div>
        </div>
      </div>
    </section>
  );
};
