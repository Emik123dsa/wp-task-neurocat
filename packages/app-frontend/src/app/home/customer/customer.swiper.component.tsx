/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import React, { Fragment } from 'react';
import SwiperCore, { Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import UserIcon from '~/assets/img/user.svg';
import { PostItemImpl } from '@/core/models/category.model';
/**
 * Customer Swiper Prop Imp
 *
 * @export
 * @interface CustomerSwiperPropImpl
 */
export interface CustomerSwiperPropImpl {
  items: PostItemImpl[];
}

SwiperCore.use([Pagination]);

export class CustomerSwiperComponent extends React.Component<CustomerSwiperPropImpl> {
  private _paginationRef: React.RefObject<HTMLDivElement> = null;

  public constructor(props: Readonly<CustomerSwiperPropImpl>) {
    super(props);
    this._paginationRef = React.createRef<HTMLDivElement>();
  }

  public render(): JSX.Element {
    const { items }: CustomerSwiperPropImpl = this.props;
    return (
      <Fragment>
        <Swiper
          observer={true}
          observeParents={true}
          spaceBetween={50}
          slidesPerView={1}
          loop={true}
          pagination={{
            clickable: true,
            type: 'bullets',
            el: this._paginationRef.current,
          }}
        >
          {items?.map((item: PostItemImpl, index: number) => (
            <SwiperSlide key={`${item.postTitle}${index}`}>
              <div className="block justify-items-center justify-center">
                <div className="text-center mb-4">
                  <UserIcon className="mx-auto" />
                </div>
                <blockquote className="max-w-lg text-center mx-auto">
                  <p className="text-sm font-medium">{item.postContent}</p>
                </blockquote>
              </div>
            </SwiperSlide>
          ))}
          <div
            ref={this._paginationRef}
            className="swiper-pagination mt-4"
          ></div>
        </Swiper>
      </Fragment>
    );
  }
}
