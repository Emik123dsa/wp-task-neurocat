import React from 'react';
import { render } from '@testing-library/react';
import { CustomerComponent } from './customer.component';
import { CategoryItemImpl } from '@/core/models/category.model';

describe('Customer', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <CustomerComponent customerItems={{} as CategoryItemImpl} />,
    );
    expect(baseElement).toBeTruthy();
  });
});
