import React from 'react';
import { TeamSwiperComponent } from './team.swiper.component';
import { CategoryItemImpl } from '@/core/models/category.model';

const NEUROCAT_TEAM_LOGO_FACTORY =
  'https://www.neurocat.ai/wp-content/themes/neurocat/img/icon-17.png';

type TeamItemImpl = CategoryItemImpl;

export interface TeamPropsImpl {
  teamItems: TeamItemImpl;
}

export const TeamComponent: React.FunctionComponent<TeamPropsImpl> = ({
  teamItems,
}: TeamPropsImpl): JSX.Element => (
  <section className="team py-20 relative">
    <div className="container px-6 mx-auto">
      <h2 className="text-white mx-auto text-center font-titillium-regular font-semibold text-xl max-w-sm mb-4">
        {teamItems?.categoryName}
      </h2>
      <div className="absolute bottom-0 right-0">
        <img
          src={NEUROCAT_TEAM_LOGO_FACTORY}
          srcSet={NEUROCAT_TEAM_LOGO_FACTORY}
          alt="neurocat-team"
          title="neurocat-team"
        />
      </div>
      <div className="team-swiper mt-6">
        <TeamSwiperComponent
          items={teamItems?.categoryChildren[0]?.categoryDetails}
        />
      </div>

      <div className="mt-6 relative">
        <p className="max-w-2xl text-xs mx-auto text-center font-medium">
          {teamItems?.categoryChildren[0]?.categoryDescription}
        </p>
      </div>
    </div>
  </section>
);
