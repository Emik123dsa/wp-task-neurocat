import React from 'react';
import { render } from '@testing-library/react';
import { TeamSwiperComponent } from './team.swiper.component';
import { PostItemImpl } from '@/core/models/category.model';

describe('Team Swiper', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <TeamSwiperComponent items={[] as PostItemImpl[]} />,
    );
    expect(baseElement).toBeTruthy();
  });
});
