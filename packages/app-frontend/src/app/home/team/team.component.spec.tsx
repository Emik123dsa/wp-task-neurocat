import React from 'react';
import { render } from '@testing-library/react';
import { TeamComponent } from './team.component';
import { CategoryItemImpl } from '@/core/models/category.model';

describe('Team', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <TeamComponent teamItems={{} as CategoryItemImpl} />,
    );
    expect(baseElement).toBeTruthy();
  });
});
