/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { StaticContext } from '@/core/models/request.initial.data.model';
import { LineStripe } from '@/shared/helpers/line.stripe.component';
import React, { Fragment } from 'react';
import { AdvantagesComponent } from './advantages/advantages.component';
import { CustomerComponent } from './customer/customer.component';
import { HeroComponent } from './hero/hero.component';
import { InfoGraphicComponent } from './infographic/infographic.component';
import { MiscellaneousComponent } from './miscellaneous/miscellaneous.component';
import { TeamComponent } from './team/team.component';
import { Helmet } from 'react-helmet';
import { LandingService } from '@/core/services/landing.service';
import { Observable } from 'rxjs/internal/Observable';
import { forkJoin, Subscription } from 'rxjs';
import { StaticAppContext } from '@/core/context/static.context';
import { ServerInitData } from '@/core/models/server.model';
import { CategoryItemImpl, PostItemImpl } from '@/core/models/category.model';
import { ServerInitialDataImpl } from '@/core/models/server.model';
import { IServerProps } from '@/core/models/server.model';
import { shareReplay } from 'rxjs/operators';

export class HomeComponent extends React.Component<
  IServerProps<StaticContext<{ appContent: CategoryItemImpl[] }>> &
    StaticContext<{ appContent: CategoryItemImpl[] }>,
  ServerInitialDataImpl
> {
  static requestInitialData(): Observable<{ appContent: CategoryItemImpl[] }> {
    const landingService: LandingService = new LandingService();
    return forkJoin({
      appContent: landingService.getAppContent(),
    });
  }

  public state: ServerInitialDataImpl = {
    appContent: null,
  };

  private _renderSiteMeta(): JSX.Element {
    const {
      location: { pathname },
    }: IServerProps<StaticContext<unknown>> = this.props;

    return (
      <Helmet
        title={HomeComponent.HOME_TITLE}
        link={[
          {
            href: pathname,
            rel: 'canonical',
          },
        ]}
      />
    );
  }

  private _appContent$: Subscription;

  public static contextType: React.Context<ServerInitData> = StaticAppContext;
  /**
   * Home title
   * environment
   *
   * @private
   * @static
   * @type {string}
   * @memberof HomeComponent
   */
  private static readonly HOME_TITLE: string = 'Home';

  public componentDidMount(): void {
    if (!this.context.appContent) {
      const landingService: LandingService = new LandingService();
      // eslint-disable-next-line @typescript-eslint/no-this-alias
      this._appContent$ = landingService
        .getAppContent()
        .pipe(shareReplay({ refCount: true, bufferSize: 2 }))
        .subscribe((data: CategoryItemImpl[]) => {
          this.setState(
            (): ServerInitialDataImpl => ({
              appContent: data,
            }),
          );
        });
    }
  }

  public componentWillUnmount(): void {
    if (this._appContent$) {
      this._appContent$.unsubscribe();
    }
  }

  public render(): JSX.Element {
    return (
      <Fragment>
        {this._renderSiteMeta()}
        <HeroComponent
          verboseItems={
            this._categoryContentByName('Hero', 'HeroVerbose') as PostItemImpl[]
          }
          titleItems={
            this._categoryContentByName('Hero', 'HeroTitle') as PostItemImpl[]
          }
        />
        <AdvantagesComponent
          advantageItems={
            this._categoryContentByName(
              'Hero',
              'HeroAdvantages',
            ) as PostItemImpl[]
          }
        />
        <LineStripe />
        <InfoGraphicComponent
          titleFeatures={
            this._categoryContentByName(
              'Features',
              'FeaturesTitle',
            ) as PostItemImpl[]
          }
          essentialFeatures={
            this._categoryContentByName(
              'Features',
              'FeaturesEssential',
            ) as PostItemImpl[]
          }
          otherFeatures={
            this._categoryContentByName(
              'Features',
              'FeaturesOther',
            ) as PostItemImpl[]
          }
        />
        <LineStripe />
        <MiscellaneousComponent
          miscellaneousItems={
            this._categoryContentByName('Miscellaneous') as CategoryItemImpl
          }
        />
        <LineStripe />
        <CustomerComponent
          customerItems={
            this._categoryContentByName('Customer') as CategoryItemImpl
          }
        />
        <LineStripe />
        <TeamComponent
          teamItems={this._categoryContentByName('Team') as CategoryItemImpl}
        />
        <LineStripe />
      </Fragment>
    );
  }

  private _categoryContentByName(
    ..._args: string[]
  ): PostItemImpl[] | CategoryItemImpl | CategoryItemImpl[] {
    const appContent: CategoryItemImpl[] =
      this.context.appContent || this.state.appContent;
    const args: string[] = [...Array.from(_args)];
    if (!appContent) return [];
    const appCategory: CategoryItemImpl =
      appContent?.find((data: CategoryItemImpl) =>
        data?.categoryName.startsWith(args[0]),
      ) || null;
    if (args.length > 1) {
      return (
        (appCategory &&
          appCategory?.categoryChildren.find((data: CategoryItemImpl) =>
            data?.categoryName.startsWith(args[1]),
          )?.categoryDetails) ||
        ([] as PostItemImpl[])
      );
    }
    return appCategory || ({} as CategoryItemImpl);
  }
}
