import React from 'react';
import WaveInverted from '~/assets/img/wave.inverted.svg';
import { PostItemImpl } from '@/core/models/category.model';
import { AdvantageItemComponent } from '@/shared/helpers/advantage-item/advantage-item.component';

export interface AdvantagePropsImpl {
  advantageItems: PostItemImpl[];
}

export const AdvantagesComponent: React.FunctionComponent<AdvantagePropsImpl> = ({
  advantageItems,
}: AdvantagePropsImpl) => (
  <section className="advantages pt-20 relative">
    <div className="w-full">
      <div
        style={{
          zIndex: -10,
        }}
        className="absolute transform -left-36 -top-44"
      >
        <WaveInverted />
      </div>
    </div>
    <div className="w-full relative block z-50">
      <div className="container px-6 mx-auto">
        <div className="pb-20">
          <ul className="flex items-center flex-wrap justify-items-center justify-around">
            {advantageItems?.map(
              (advantageItem: PostItemImpl, index: number) => (
                <AdvantageItemComponent
                  key={`${advantageItem.postTitle}${index}`}
                  item={advantageItem}
                />
              ),
            )}
          </ul>
        </div>
      </div>
    </div>
  </section>
);
