import React from 'react';
import { render } from '@testing-library/react';
import { HomeComponent } from './home.component';

describe('App', () => {
  it('should render successfully', () => {
    const props: Record<string, unknown> = {};
    const { baseElement } = render(<HomeComponent staticContext={props} />);
    expect(baseElement).toBeTruthy();
  });

  // it('should have a greeting as the title', () => {
  //   const { getByText } = render(<HomeComponent />);

  //   expect(getByText('Welcome to hello!')).toBeTruthy();
  // });
});
