import React from 'react';
import { render } from '@testing-library/react';
import { MiscellaneousComponent } from './miscellaneous.component';

describe('Infographic', () => {
  it('it should be sucessfully rendered', () => {
    const { baseElement } = render(<MiscellaneousComponent />);
    expect(baseElement).toBeTruthy();
  });
});
