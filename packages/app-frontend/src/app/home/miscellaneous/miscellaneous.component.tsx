import { CategoryItemImpl, PostItemImpl } from '@/core/models/category.model';
import React from 'react';
import WaveInverted from '~/assets/img/wave.inverted.svg';
/**
 * Miscellaneous props impl
 *
 * @export
 * @interface MiscellaneousPropsImpl
 */
export interface MiscellaneousPropsImpl {
  miscellaneousItems: CategoryItemImpl;
}

export const MiscellaneousComponent: React.FunctionComponent<MiscellaneousPropsImpl> = ({
  miscellaneousItems,
}: MiscellaneousPropsImpl): JSX.Element => {
  return (
    <section className="miscellaneous py-20 relative">
      <div className="w-full">
        <div
          style={{
            zIndex: -10,
          }}
          className="absolute transform -left-36 -top-44"
        >
          <WaveInverted />
        </div>
      </div>
      <div className="container px-6 mx-auto">
        {miscellaneousItems?.categoryDetails?.map(
          (item: PostItemImpl, index: number) => (
            <div
              key={`${item.postTitle}${index}`}
              className={`w-full ${index > 0 ? 'pt-20' : 'pt-0'}`}
            >
              <div
                className={`flex items-center justify-items-center justify-between ${
                  index % 2 !== 0 ? 'flex-row-reverse' : 'flex-row'
                }`}
              >
                <div className="pointer-events-none select-none">
                  <img
                    src={item.postDescription || null}
                    srcSet={item.postDescription || null}
                    alt={item.postTitle.toLowerCase()}
                    title={item.postTitle.toLowerCase()}
                  />
                </div>
                <div className="mt-0">
                  <h4 className="max-w-md font-thin text-md text-white mb-4">
                    {item.postTitle}
                  </h4>
                  <div className="max-w-lg">
                    <p className="font-heebo-regular font-thin">
                      {item.postContent}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          ),
        )}
      </div>
    </section>
  );
};
