import React, { Component, Fragment } from 'react';
import { renderRoutes } from 'react-router-config';
import { FooterComponent } from '@/shared/layouts/footer/footer.component';
import { HeaderComponent } from '@/shared/layouts//header/header.component';
import { ɵroutes } from '@/app.routing-module';

export class AppComponent extends Component<unknown, unknown> {
  public render(): JSX.Element {
    return (
      <Fragment>
        <div className="fixed top-0 left-0 w-full h-full bg-overlay"></div>
        <div className="mx-auto shadow-width bg-background min-h-screen relative z-50 overflow-hidden">
          <HeaderComponent />
          <main className="mx-auto px-0 xl:px-width">
            {renderRoutes(ɵroutes)}
          </main>
          <FooterComponent />
        </div>
      </Fragment>
    );
  }
}
