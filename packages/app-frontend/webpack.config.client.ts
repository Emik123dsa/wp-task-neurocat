import * as path from 'path';
import * as webpack from 'webpack';
import { resolve, clientPlugins, entryClientFiles } from './utils';

const PORT: number = Number(process.env.NODE_PORT) || 4200;
let webpackClientPort: number = PORT;
webpackClientPort = ++webpackClientPort;

const IS_DEV_SERVER: boolean = process.env.NODE_DEV_SERVER === 'true';

export const outputClient: (isDev: boolean) => webpack.Configuration = (
  isDev: boolean,
) => ({
  output: {
    filename: isDev ? '[chunkhash:4].dev.js' : '[contenthash].js',
    chunkFilename: isDev ? '[chunkhash:4].dev.js' : '[contenthash].js',
    path: path.join(process.cwd(), 'dist/client'),
    publicPath: !IS_DEV_SERVER
      ? isDev
        ? `//localhost:${webpackClientPort}/`
        : '/'
      : '/',
  },
});

export const configClient: (isDev: boolean) => webpack.Configuration = (
  isDev: boolean,
) => ({
  mode: isDev ? 'development' : 'production',
  bail: true,
  devtool: isDev ? 'inline-source-map' : undefined,
  performance: {
    hints: false,
  },
  entry: []
    .concat(
      IS_DEV_SERVER
        ? ['webpack/hot/only-dev-server', 'react-hot-loader/patch']
        : [],
    )
    .concat(entryClientFiles),
  target: isDev ? 'web' : 'browserslist',
  resolve,
  plugins: clientPlugins,
  stats: 'errors-only',
});
