/* eslint-disable */
import merge from 'webpack-merge';
import * as webpack from 'webpack';
import { outputClient, configClient } from './webpack.config.client';
import { moduleLoader, optimization, devServer } from './utils';
import { configServer, outputServer } from './webpack.config.server';

interface __SSR__ {
  client: webpack.Configuration;
  server: webpack.Configuration;
}

const IS_NODE_DEV: boolean = process.env.NODE_ENV === 'development';
const IS_DEV_SERVER: boolean = process.env.NODE_DEV_SERVER === 'true';

const __CLIENT__: webpack.Configuration = merge(
  moduleLoader('client'),
  outputClient(IS_NODE_DEV),
  configClient(IS_NODE_DEV),
  IS_DEV_SERVER ? (IS_NODE_DEV ? devServer : {}) : {},
  !IS_NODE_DEV ? optimization : {},
);

const IS_SSR: boolean = process.env.NODE_SSR === 'true';

const __SERVER__: webpack.Configuration = merge(
  moduleLoader('server'),
  outputServer(IS_NODE_DEV),
  configServer(IS_NODE_DEV),
  !IS_NODE_DEV ? optimization : {},
);

export default IS_NODE_DEV
  ? IS_SSR
    ? ({ client: __CLIENT__, server: __SERVER__ } as __SSR__)
    : __CLIENT__
  : [__CLIENT__, __SERVER__];
