/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import * as webpack from 'webpack';
import * as path from 'path';
import { entryServerFiles } from './utils';
import { resolve, serverPlugins } from './utils';
import WebpackNodeExternals from 'webpack-node-externals';

export const outputServer: (isDev: boolean) => webpack.Configuration = (
  isDev: boolean,
) => ({
  output: {
    filename: isDev ? 'server.js' : '[contenthash].[name].js',
    chunkFilename: isDev ? '[chunkhash:4].server.js' : '[contenthash].js',
    path: path.join(process.cwd(), 'dist/server'),
    publicPath: '/',
    library: 'app',
    libraryTarget: 'commonjs2',
  },
});

export const configServer: (isDev: boolean) => webpack.Configuration = (
  isDev: boolean,
): webpack.Configuration => ({
  mode: isDev ? 'development' : 'production',
  bail: true,
  entry: [...entryServerFiles],
  devtool: isDev ? 'eval-source-map' : undefined,
  target: 'node',
  node: {
    __dirname: true,
    __filename: true,
  },
  performance: {
    hints: false,
  },
  externals: [WebpackNodeExternals({})],
  resolve: {
    ...resolve,
    modules: [path.resolve(process.cwd(), 'src')],
  },
  plugins: [...serverPlugins],
  stats: 'errors-only',
});
